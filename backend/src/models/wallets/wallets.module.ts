import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Wallet } from './wallets.entity'

@Module({
  imports: [TypeOrmModule.forFeature([Wallet])],
  exports: [TypeOrmModule],
})
export class WalletsEntityModule {}
