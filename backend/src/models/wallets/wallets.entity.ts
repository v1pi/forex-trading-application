import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'
import { BaseModel } from '../basis/base.entity'
import { WalletInterface } from './wallets.interface'

@Entity('wallets')
export class Wallet extends BaseModel<Wallet> implements WalletInterface {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('text', { nullable: false })
  email!: string

  @Column('text', { nullable: false })
  uid!: string

  @Column('text', { nullable: false })
  name!: string

  @Column('double precision', { nullable: false })
  amount!: number

  fillFromJson(json: any): Wallet {
    if (!json) {
      return this
    }
    this.id = json.id
    this.email = json.email as string
    this.name = json.name as string
    this.amount = json.amount as number
    this.uid = json.uid as string
    return this
  }

  public static fromJson(json: any): Wallet {
    return new Wallet().fillFromJson(json)
  }
}
