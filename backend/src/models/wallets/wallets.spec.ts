import { Wallet } from './wallets.entity'

describe('wallet', () => {
  it('test json wallet', () => {
    const mockWallet = new Wallet()
    mockWallet.id = 1
    mockWallet.name = 'Vinicius Picanço'
    mockWallet.email = 'vimivini99@gmail.com'
    mockWallet.amount = 100
    mockWallet.uid = 'vdZ83Oifh0XsUDFjsNeYVgQ7vZA3'
    expect(JSON.parse(JSON.stringify(mockWallet))).toStrictEqual(
      JSON.parse(JSON.stringify(expectedJSON())),
    )
    expect(mockWallet).toStrictEqual(Wallet.fromJson(expectedJSON()))
  })
})

function expectedJSON() {
  return {
    id: 1,
    name: 'Vinicius Picanço',
    email: 'vimivini99@gmail.com',
    uid: 'vdZ83Oifh0XsUDFjsNeYVgQ7vZA3',
    amount: 100,
  }
}
