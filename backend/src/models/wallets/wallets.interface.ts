export interface WalletInterface {
  email: string
  name: string
  amount: number
  uid: string
}
