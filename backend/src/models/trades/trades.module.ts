import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Trade } from './trades.entity'

@Module({
  imports: [TypeOrmModule.forFeature([Trade])],
  exports: [TypeOrmModule],
})
export class TradesEntityModule {}
