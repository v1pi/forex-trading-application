import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'
import { BaseModel } from '../basis/base.entity'
import { TradeInterface, TradeStatus, TradeType } from './trades.interface'
import * as moment from 'moment-timezone'
@Entity('trades')
export class Trade extends BaseModel<Trade> implements TradeInterface {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('double precision', { nullable: false })
  amount!: number

  @Column('double precision', { nullable: true })
  profit!: number

  @Column('double precision', { nullable: false })
  uid!: string

  @Column('double precision', { nullable: false })
  multiplier!: number

  @Column('int', { nullable: false })
  type!: TradeType

  @Column('int', { nullable: false })
  status!: TradeStatus

  @Column('double precision', { nullable: false })
  open!: number

  @Column('double precision', { nullable: false })
  closed!: number

  @Column('timestamptz', { nullable: false, name: 'date_open' })
  dateOpen!: Date

  @Column('timestamptz', { nullable: false, name: 'date_closed' })
  dateClosed!: Date

  fillFromJson(json: any): Trade {
    if (!json) {
      return this
    }
    this.id = json.id
    this.multiplier = json.multiplier as number
    this.amount = json.amount as number
    this.profit = json.profit as number
    this.uid = json.uid as string
    this.type = json.type as TradeType
    this.status = json.status as TradeStatus
    this.open = json.open as number
    this.closed = json.closed as number
    this.dateOpen = moment(json.dateOpen).toDate()
    this.dateClosed = moment(json.dateClosed).toDate()
    return this
  }

  public static fromJson(json: any): Trade {
    return new Trade().fillFromJson(json)
  }
}
