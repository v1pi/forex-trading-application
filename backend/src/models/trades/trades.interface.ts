export enum TradeStatus {
  OPEN = 0,
  CLOSE = 1,
}

export enum TradeType {
  SELL = 0,
  BUY = 1,
}

export interface TradeInterface {
  amount: number
  profit: number
  uid: string
  multiplier: number
  type: TradeType
  status: TradeStatus
  open: number
  closed: number
  dateOpen: Date
  dateClosed: Date
}
