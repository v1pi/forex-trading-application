import * as moment from 'moment'
import { Trade } from './trades.entity'
import { TradeStatus } from './trades.interface'

describe('trades', () => {
  it('test json trades', () => {
    const mockTrade = new Trade()
    mockTrade.amount = 250
    mockTrade.multiplier = 20
    mockTrade.id = 1
    mockTrade.type = 1
    mockTrade.closed = 1.5
    mockTrade.profit = -500
    mockTrade.open = 1.6
    mockTrade.status = TradeStatus.CLOSE
    mockTrade.uid = 'vdZ83Oifh0XsUDFjsNeYVgQ7vZA3'
    mockTrade.dateOpen = moment('2021-04-09T11:44:28.370Z').toDate()
    mockTrade.dateClosed = moment('2021-04-09T11:47:50.294Z').toDate()
    expect(JSON.parse(JSON.stringify(mockTrade))).toStrictEqual(
      JSON.parse(JSON.stringify(expectedJSON())),
    )
    expect(mockTrade).toStrictEqual(Trade.fromJson(expectedJSON()))
  })
})

function expectedJSON() {
  return {
    amount: 250,
    id: 1,
    multiplier: 20,
    type: 1,
    status: 1,
    uid: 'vdZ83Oifh0XsUDFjsNeYVgQ7vZA3',
    dateOpen: '2021-04-09T11:44:28.370Z',
    open: 1.6,
    closed: 1.5,
    dateClosed: '2021-04-09T11:47:50.294Z',
    profit: -500,
  }
}
