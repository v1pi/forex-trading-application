export interface ForexInterface {
  close: number
  high: number
  open: number
  low: number
  dateTime: Date
}
