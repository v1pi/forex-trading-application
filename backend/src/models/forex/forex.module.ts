import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Forex } from './forex.entity'

@Module({
  imports: [TypeOrmModule.forFeature([Forex])],
  exports: [TypeOrmModule],
})
export class ForexEntityModule {}
