import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'
import { BaseModel } from '../basis/base.entity'
import { ForexInterface } from './forex.interface'
import * as moment from 'moment-timezone'

@Entity('forex')
export class Forex extends BaseModel<Forex> implements ForexInterface {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('double precision', { nullable: false })
  close!: number

  @Column('double precision', { nullable: false })
  high!: number

  @Column('double precision', { nullable: false })
  open!: number

  @Column('double precision', { nullable: false })
  low!: number

  @Column('timestamptz', { nullable: false, name: 'date_time' })
  dateTime!: Date

  fillFromJson(json: any): Forex {
    if (!json) {
      return this
    }
    this.id = json.id
    this.low = json.low as number
    this.close = json.close as number
    this.high = json.high as number
    this.open = json.open as number
    this.dateTime = moment(json.dateTime).toDate()
    return this
  }

  public static fromJson(json: any): Forex {
    return new Forex().fillFromJson(json)
  }
}
