import * as moment from 'moment-timezone'
import { Forex } from './forex.entity'

describe('Forex', () => {
  it('test json Forex', () => {
    const mockUsuario = new Forex()
    mockUsuario.id = 1
    mockUsuario.close = 1.5
    mockUsuario.open = 1.4
    mockUsuario.high = 1.6
    mockUsuario.low = 1.3
    mockUsuario.dateTime = moment('2020-01-23T02:23:45.678-03:00').toDate()
    expect(JSON.parse(JSON.stringify(mockUsuario))).toStrictEqual(
      JSON.parse(JSON.stringify(expectedJSON())),
    )
    expect(mockUsuario).toStrictEqual(Forex.fromJson(expectedJSON()))
  })
})

function expectedJSON() {
  return {
    id: 1,
    close: 1.5,
    open: 1.4,
    high: 1.6,
    low: 1.3,
    dateTime: moment('2020-01-23T02:23:45.678-03:00').tz(moment.tz.guess()),
  }
}
