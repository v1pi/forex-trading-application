import { Test, TestingModule } from '@nestjs/testing'
import { RedisConfigService } from './config.service'
import { RedisConfigModule } from './config.module'

jest.mock('dotenv')
jest.mock('fs')
describe('RedisConfigService', () => {
  let service: RedisConfigService

  beforeEach(async () => {
    process.env = {
      REDIS_HOST: '0.0.0.0',
      REDIS_PORT: '5432',
    }
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [RedisConfigModule],
    }).compile()

    service = moduleRef.get<RedisConfigService>(RedisConfigService)
  })

  it('should be read right values', () => {
    expect(service).toBeDefined()
    expect(service.port).toBe(5432)
    expect(service.host).toBe('0.0.0.0')
  })
})
