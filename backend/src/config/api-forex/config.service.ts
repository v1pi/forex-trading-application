import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
/**
 * Service dealing with app config based operations.
 *
 * @class
 */
@Injectable()
export class ApiForexConfigService {
  constructor(private configService: ConfigService) {}

  get host(): string {
    return this.configService.get<string>('api-forex.host')
  }

  get key(): string {
    return this.configService.get<string>('api-forex.key')
  }
}
