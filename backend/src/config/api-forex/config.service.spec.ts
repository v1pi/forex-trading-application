import { Test, TestingModule } from '@nestjs/testing'
import { ApiForexConfigService } from './config.service'
import { ApiForexConfigModule } from './config.module'

jest.mock('dotenv')
jest.mock('fs')
describe('ApiForexConfigService', () => {
  let service: ApiForexConfigService

  beforeEach(async () => {
    process.env = {
      API_FOREX_KEY: 'key',
      API_FOREX_HOST: 'host',
    }
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ApiForexConfigModule],
    }).compile()

    service = moduleRef.get<ApiForexConfigService>(ApiForexConfigService)
  })

  it('should be read right values', () => {
    expect(service).toBeDefined()
    expect(service.key).toBe('key')
    expect(service.host).toBe('host')
  })
})
