import * as Joi from '@hapi/joi'
import { Module } from '@nestjs/common'
import configuration from './configuration'
import { ApiForexConfigService } from './config.service'
import { ConfigModule, ConfigService } from '@nestjs/config'
/**
 * Import and provide app configuration related classes.
 *
 * @module
 */
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      envFilePath: `${process.env.NODE_ENV || 'development'}.env`,
      validationSchema: Joi.object({
        API_FOREX_HOST: Joi.string().default(
          'https://marketdata.tradermade.com/api/v1/',
        ),
        API_FOREX_KEY: Joi.string().default('zoJ0BK4R6Kd-8xffKEeL'),
      }),
    }),
  ],
  providers: [ConfigService, ApiForexConfigService],
  exports: [ConfigService, ApiForexConfigService],
})
export class ApiForexConfigModule {}
