import { registerAs } from '@nestjs/config'
export default registerAs('api-forex', () => ({
  host: process.env.API_FOREX_HOST,
  key: process.env.API_FOREX_KEY,
}))
