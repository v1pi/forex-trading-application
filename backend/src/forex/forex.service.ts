import { HttpService, Injectable } from '@nestjs/common'
import { ApiForexConfigService } from '../config/api-forex/config.service'
import * as moment from 'moment-timezone'
import { InjectRepository } from '@nestjs/typeorm'
import { MoreThan, Repository } from 'typeorm'
import { Forex } from 'src/models/forex/forex.entity'

@Injectable()
export class ForexService {
  constructor(
    @InjectRepository(Forex) private forexDoc: Repository<Forex>,
    private httpService: HttpService,
    private apiForexConfig: ApiForexConfigService,
  ) {}
  async getAllForexFromLastTwoHours(): Promise<Forex[]> {
    const lastTwoHours = moment().subtract(2, 'hours').toDate()
    return this.forexDoc.find({
      where: { dateTime: MoreThan(lastTwoHours) },
      order: { dateTime: 'ASC' },
    })
  }

  async newForex(forex: Forex): Promise<Forex> {
    return this.forexDoc.save(forex)
  }

  async insertMany(forex: Forex[]): Promise<Forex[]> {
    return this.forexDoc.save(forex)
  }

  async getLastForexValue(): Promise<Forex[]> {
    return this.forexDoc.find({ order: { id: 'DESC' }, take: 1 })
  }

  async downloadForexUntilNow(startDate: moment.Moment): Promise<Forex[]> {
    const listForex = []
    startDate = startDate.utc()
    const endDate = moment().utc()
    const response = await this.httpService
      .get(
        `${this.apiForexConfig.host}/timeseries?currency=GBPUSD&api_key=${
          this.apiForexConfig.key
        }&start_date=${startDate.format(
          'YYYY-MM-DD-HH:mm',
        )}&end_date=${endDate.format(
          'YYYY-MM-DD-HH:mm',
        )}&format=records&interval=minute&period=15`,
      )
      .toPromise()
    for (const forexData of response.data.quotes) {
      const forex = new Forex()
      forex.close = forexData.close
      forex.open = forexData.open
      forex.high = forexData.high
      forex.low = forexData.low
      forex.dateTime = moment.utc(forexData.date).toDate()
      listForex.push(forex)
    }

    return listForex
  }

  async downloadForexLastFifteenMinutes(): Promise<Forex> {
    const startDate = moment().subtract('15', 'minutes').utc()
    const endDate = moment().utc()
    const response = await this.httpService
      .get(
        `${this.apiForexConfig.host}/timeseries?currency=GBPUSD&api_key=${
          this.apiForexConfig.key
        }&start_date=${startDate.format(
          'YYYY-MM-DD-HH:mm',
        )}&end_date=${endDate.format(
          'YYYY-MM-DD-HH:mm',
        )}&format=records&interval=minute&period=15`,
      )
      .toPromise()
    const forexData = response.data
    const lastPos = forexData.quotes.length - 1
    const forex = new Forex()
    forex.close = forexData.quotes[lastPos].close
    forex.high = forexData.quotes[lastPos].high
    forex.low = forexData.quotes[lastPos].low
    forex.open = forexData.quotes[lastPos].open
    forex.dateTime = moment.utc(forexData.quotes[lastPos].date).toDate()

    return forex
  }
}
