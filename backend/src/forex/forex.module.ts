import { HttpModule, Module } from '@nestjs/common'
import { ApiForexConfigModule } from 'src/config/api-forex/config.module'
import { ForexEntityModule } from 'src/models/forex/forex.module'
import { ForexGateway } from './forex.gateway'
import { ForexService } from './forex.service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    ForexEntityModule,
    ApiForexConfigModule,
  ],
  providers: [ForexGateway, ForexService],
})
export class ForexModule {}
