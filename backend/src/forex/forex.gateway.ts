import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets'
import { from, Observable } from 'rxjs'
import { Server } from 'socket.io'
import { map } from 'rxjs/operators'
import { Logger, UseFilters } from '@nestjs/common'
import { AllExceptionsFilterWs } from '../common/exceptions/ws/all-exceptions-ws.filter'
import { ForexService } from './forex.service'
import { Cron, SchedulerRegistry } from '@nestjs/schedule'
import * as moment from 'moment-timezone'
import { Mutex } from 'async-mutex'
import { Forex } from 'src/models/forex/forex.entity'
@WebSocketGateway()
export class ForexGateway {
  private readonly logger = new Logger(ForexGateway.name)
  private readonly mutex = new Mutex()
  constructor(
    private forexService: ForexService,
    private schedulerRegistry: SchedulerRegistry,
  ) {}

  async getAllForexFromLastTwoHours(): Promise<Forex[]> {
    const release = await this.mutex.acquire()
    const listForex = await this.forexService.getAllForexFromLastTwoHours()
    let startDate = moment().subtract(2, 'hours').utc()
    if (listForex.length !== 0) {
      startDate = moment(listForex[listForex.length - 1].dateTime).utc()
    }
    if (
      moment().utc().subtract(30, 'minutes').isAfter(startDate) &&
      moment().utc().day() !== 6 &&
      moment().utc().day() !== 7
    ) {
      const newEntries = await this.forexService.downloadForexUntilNow(
        startDate,
      )

      await this.forexService.insertMany(newEntries)
      listForex.push(...newEntries)
    }
    release()
    return listForex
  }

  @WebSocketServer()
  server: Server
  @UseFilters(new AllExceptionsFilterWs())
  @SubscribeMessage('forex')
  async findAll(): Promise<Observable<WsResponse<Forex[]>>> {
    return from(this.getAllForexFromLastTwoHours()).pipe(
      map((doc) => ({
        event: 'forex',
        data: doc,
      })),
    )
  }

  @Cron('*/15 * * * *', {
    name: 'updateForex',
  })
  async triggerUpdateForex(): Promise<void> {
    const temp = await this.forexService.downloadForexLastFifteenMinutes()
    const forex = await this.forexService.newForex(temp)
    this.server.emit('forex', [forex])
  }
}
