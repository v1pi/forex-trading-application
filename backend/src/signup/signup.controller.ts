import { FirebaseAuthenticationService } from '@aginix/nestjs-firebase-admin'
import { Body, Controller, Post } from '@nestjs/common'
import { ErrorType } from '../common/enums/error-type.enum'
import { UserType } from '../common/enums/user-type.enum'
import { ResponseDefault } from '../common/interfaces/response-default.interface'
import { WalletsService } from '../wallets/wallets.service'
import { CreateUserDto } from './dto/create-user.dto'
@Controller('signup')
export class SignUpController {
  constructor(
    private serv: WalletsService,
    private auth: FirebaseAuthenticationService,
  ) {}

  @Post('admin')
  public async registerCliente(
    @Body() newUserDto: CreateUserDto,
  ): Promise<ResponseDefault> {
    const user = await this.auth.createUser({
      email: newUserDto.email,
      password: newUserDto.password,
      displayName: newUserDto.name,
    })
    await this.auth.setCustomUserClaims(user.uid, {
      role: UserType.ADMIN,
    })
    newUserDto.uid = user.uid
    newUserDto.amount = 0
    try {
      delete newUserDto.password
      const wallet = await this.serv.create(newUserDto)
      return {
        error_id: ErrorType.NO_ERRORS,
        message: 'Success!',
        error: false,
        data: {
          wallet,
        },
      }
    } catch (error) {
      this.auth.deleteUser(user.uid)
      throw error
    }
  }
}
