import { Module } from '@nestjs/common'
import { WalletsEntityModule } from 'src/models/wallets/wallets.module'
import { WalletsService } from '../wallets/wallets.service'
import { SignUpController } from './signup.controller'

@Module({
  imports: [WalletsEntityModule],
  controllers: [SignUpController],
  providers: [WalletsService],
})
export class SignUpModule {}
