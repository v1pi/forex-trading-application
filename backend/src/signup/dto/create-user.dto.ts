import { IsEmail, IsOptional, IsString, Length } from 'class-validator'

export class CreateUserDto {
  @IsString()
  @Length(1)
  @IsEmail()
  email: string
  @IsString()
  @Length(1)
  password: string
  @IsString()
  @Length(1)
  name: string
  @IsOptional()
  uid: string
  @IsOptional()
  amount: number
}
