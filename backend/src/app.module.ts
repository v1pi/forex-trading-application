import {
  MiddlewareConsumer,
  Module,
  RequestMethod,
  ValidationPipe,
} from '@nestjs/common'
import { FirebaseAdminModule } from '@aginix/nestjs-firebase-admin'
import * as admin from 'firebase-admin'
import { FIREBASE_CONFIG } from './common/constants/firebase'
import { AppConfigModule } from './config/app/config.module'
import { AuthMiddleware } from './common/middlewares/auth.middleware'
import { APP_FILTER, APP_GUARD, APP_PIPE } from '@nestjs/core'
import { AllExceptionsFilter } from './common/exceptions/all-exceptions.filter'
import { RolesGuard } from './common/guards/roles.guard'
import { ForexModule } from './forex/forex.module'
import { ScheduleModule } from '@nestjs/schedule'
import { ApiForexConfigModule } from './config/api-forex/config.module'
import { SignUpModule } from './signup/signup.module'
import { WalletsModule } from './wallets/wallets.module'
import { TradesModule } from './trades/trades.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PgModelsConfigModule } from './config/postgressql/config.module'
import { PgModelsConfigService } from './config/postgressql/config.service'
import { BullModule } from '@nestjs/bull'
import { RedisConfigModule } from './config/redis/config.module'
import { RedisConfigService } from './config/redis/config.service'

@Module({
  imports: [
    FirebaseAdminModule.forRootAsync({
      useFactory: () => ({
        credential: admin.credential.cert(FIREBASE_CONFIG),
      }),
    }),
    TypeOrmModule.forRootAsync({
      imports: [PgModelsConfigModule],
      inject: [PgModelsConfigService],
      useFactory: async (configService: PgModelsConfigService) =>
        configService.getTypeORMConfig,
    }),
    BullModule.forRootAsync({
      imports: [RedisConfigModule],
      inject: [RedisConfigService],
      useFactory: async (configService: RedisConfigService) => ({
        redis: {
          host: configService.host,
          port: configService.port,
        },
      }),
    }),
    AppConfigModule,
    ApiForexConfigModule,
    ForexModule,
    SignUpModule,
    TradesModule,
    WalletsModule,
    ScheduleModule.forRoot(),
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude('signup')
      .forRoutes({ path: '*', method: RequestMethod.ALL })
  }
}
