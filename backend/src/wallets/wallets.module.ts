import { Module } from '@nestjs/common'
import { WalletsEntityModule } from 'src/models/wallets/wallets.module'
import { WalletsController } from './wallets.controller'
import { WalletsService } from './wallets.service'

@Module({
  imports: [WalletsEntityModule],
  controllers: [WalletsController],
  providers: [WalletsService],
})
export class WalletsModule {}
