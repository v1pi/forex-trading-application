import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Wallet } from 'src/models/wallets/wallets.entity'
import { DeepPartial, Repository } from 'typeorm'

@Injectable()
export class WalletsService {
  constructor(
    @InjectRepository(Wallet) private walletDoc: Repository<Wallet>,
  ) {}
  public async create(wallet: DeepPartial<Wallet>): Promise<Wallet> {
    return this.walletDoc.save(wallet)
  }

  public async getByUID(uid: string): Promise<Wallet> {
    return this.walletDoc.findOne({ uid: uid })
  }

  public async updateAmount(uid: string, value: number): Promise<Wallet> {
    const newDoc = await this.walletDoc.findOne({ uid: uid })
    const newAmount = Number(newDoc.amount) + Number(value)
    await this.walletDoc.update({ uid }, { amount: newAmount })
    return this.walletDoc.findOne({ uid: uid })
  }
}
