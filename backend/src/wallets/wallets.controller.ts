import { Controller, Get, Param, Patch } from '@nestjs/common'
import { User } from 'src/common/decorators/user.decorator'
import { ErrorType } from 'src/common/enums/error-type.enum'
import { ResponseDefault } from 'src/common/interfaces/response-default.interface'
import { WalletsService } from './wallets.service'
import * as admin from 'firebase-admin'
import { AllException } from 'src/common/exceptions/all.exception'
import { Wallet } from 'src/models/wallets/wallets.entity'

@Controller('wallets')
export class WalletsController {
  constructor(private serv: WalletsService) {}
  @Get(':uid/info')
  public async getInfo(
    @Param('uid') uid: string,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    if (user.uid !== uid) {
      throw new AllException(ErrorType.USER_NO_PERMISSION)
    }
    let wallet = await this.serv.getByUID(user.uid)
    if (!wallet) {
      const newWallet = new Wallet()
      newWallet.email = user.email
      newWallet.name = user.displayName
      newWallet.uid = user.uid
      newWallet.amount = 0
      wallet = await this.serv.create(newWallet)
    }
    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        wallet,
      },
    }
  }

  @Patch(':uid/:amount/withdraw')
  public async withdraw(
    @Param('uid') uid: string,
    @Param('amount') amount: number,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    if (user.uid !== uid) {
      throw new AllException(ErrorType.USER_NO_PERMISSION)
    }
    const temp = await this.serv.getByUID(user.uid)
    if (amount <= 0) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        'You can not withdraw negative or zero amount.',
      )
    }
    if (temp.amount < amount) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        "You don't have this money amount to withdraw.",
      )
    }
    const wallet = await this.serv.updateAmount(uid, -amount)
    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        wallet,
      },
    }
  }

  @Patch(':uid/:amount/deposit')
  public async deposit(
    @Param('uid') uid: string,
    @Param('amount') amount: number,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    if (user.uid !== uid) {
      throw new AllException(ErrorType.USER_NO_PERMISSION)
    }
    if (amount <= 0) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        'You can not deposit negative or zero amount.',
      )
    }
    const wallet = await this.serv.updateAmount(uid, amount)
    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        wallet,
      },
    }
  }
}
