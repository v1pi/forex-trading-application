import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { FirebaseAuthenticationService } from '@aginix/nestjs-firebase-admin'
import { Reflector } from '@nestjs/core'
import { Claims } from './interfaces/claims.interface'
import { AllException } from '../exceptions/all.exception'
import { ErrorType } from '../enums/error-type.enum'
import { AppConfigService } from '../../config/app/config.service'
import { RequestAuth } from '../interfaces/request-auth.interface'

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private firebaseAuth: FirebaseAuthenticationService,
    private reflector: Reflector,
    private appConfig: AppConfigService,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    // Lê as roles permitidas daquela requisição em específico para o método
    const roles = this.reflector.get<number[]>('roles', context.getHandler())
    if (!roles || roles.includes(-1)) {
      return true
    }
    const ctx = context.switchToHttp()
    const request = ctx.getRequest<RequestAuth>()
    const auth = request.user

    // Verifica se aquele usuário possui um customClaim
    if (auth && auth.customClaims !== undefined) {
      const claims = auth.customClaims as Claims
      // Verifica realmente se tem a permissão para determinado método
      if (roles.some((r) => r == claims.role)) {
        return true
      } else {
        throw new AllException(ErrorType.USER_NO_PERMISSION)
      }
    }

    throw new AllException(ErrorType.NO_AUTHENTICATED)
  }
}
