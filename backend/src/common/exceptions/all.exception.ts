import { HttpException, HttpStatus } from '@nestjs/common'
import { ErrorType } from '../enums/error-type.enum'
import { COMMON_ERRORS } from '../constants/exceptions'
import { ErrorTypeDados } from './interfaces/tipo-erro-dados.interface'

export class AllException extends HttpException {
  public tipoErroDado: ErrorTypeDados
  constructor(errorId: ErrorType, message?: string, httpStatus?: HttpStatus) {
    let newErrorTypeDado: ErrorTypeDados =
      message && httpStatus
        ? { errorId: errorId, message, httpStatus }
        : message
        ? {
            errorId: errorId,
            message,
            httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
          }
        : {
            errorId,
            message: 'Unknown error',
            httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
          }

    for (const error of COMMON_ERRORS) {
      if (errorId == error.errorId) {
        newErrorTypeDado = error
      }
    }
    if (message) {
      newErrorTypeDado.message = message
    }
    super(newErrorTypeDado.message, newErrorTypeDado.httpStatus)
    this.tipoErroDado = newErrorTypeDado
  }
}
