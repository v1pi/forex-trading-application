import {
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  WsExceptionFilter,
} from '@nestjs/common'
import { AllExceptionWs } from './all.exception-ws'
import { ErrorType } from '../../enums/error-type.enum'
import { BaseWsExceptionFilter, WsException } from '@nestjs/websockets'
import SocketIO from 'socket.io'

@Catch()
export class AllExceptionsFilterWs extends BaseWsExceptionFilter {
  catch(exception: unknown, argument: ArgumentsHost) {
    const ws = argument.switchToWs()
    const socket = ws.getClient()
    if (exception instanceof AllExceptionWs) {
      socket.emit('exception', {
        status: 'error',
        message: exception.message,
      })
    } else {
      let message = 'Server internal error'
      if (exception instanceof WsException) {
        const response = exception.getError()
        if (response instanceof Object) {
          const msg = response['message']
          if (msg instanceof Array) {
            message = response['message'][0]
          } else {
            message = response['message']
          }
        } else {
          message = response
        }
      }
      socket.emit('exception', {
        status: 'error',
        message: message,
      })
    }
  }
}
