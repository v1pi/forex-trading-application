import { Test, TestingModule } from '@nestjs/testing'
import { HttpStatus } from '@nestjs/common'

import { AllExceptionWs } from './all.exception-ws'
import { AllExceptionsFilterWs } from './all-exceptions-ws.filter'

const mockJson = jest.fn()
const mockStatus = jest.fn().mockImplementation(() => ({
  json: mockJson,
}))
const mockGetResponse = jest.fn().mockImplementation(() => ({
  status: mockStatus,
}))
const mockHttpArgumentsHost = jest.fn().mockImplementation(() => ({
  getResponse: mockGetResponse,
  getRequest: jest.fn(),
}))
const mockEmit = jest.fn()
const mockGetClient = jest.fn().mockImplementation(() => ({
  emit: mockEmit,
}))
const mockSwitchToWs = jest.fn().mockImplementation(() => ({
  getClient: mockGetClient,
}))

const mockArgumentsHost = {
  switchToHttp: mockHttpArgumentsHost,
  getArgByIndex: jest.fn(),
  getArgs: jest.fn(),
  getType: jest.fn(),
  switchToRpc: jest.fn(),
  switchToWs: mockSwitchToWs,
}

describe('System header validation service', () => {
  let service: AllExceptionsFilterWs

  beforeEach(async () => {
    jest.clearAllMocks()
    const module: TestingModule = await Test.createTestingModule({
      providers: [AllExceptionsFilterWs],
    }).compile()
    service = module.get<AllExceptionsFilterWs>(AllExceptionsFilterWs)
  })

  describe('All exception filter tests', () => {
    it('should be defined', () => {
      expect(service).toBeDefined()
    })

    it('All exception', () => {
      service.catch(
        new AllExceptionWs(-20, 'Teste', HttpStatus.INTERNAL_SERVER_ERROR),
        mockArgumentsHost,
      )
      expect(mockSwitchToWs).toBeCalledTimes(1)
      expect(mockSwitchToWs).toBeCalledWith()
      expect(mockGetClient).toBeCalledTimes(1)
      expect(mockGetClient).toBeCalledWith()
      expect(mockEmit).toBeCalledTimes(1)
      expect(mockEmit).toBeCalledWith('exception', {
        status: 'error',
        message: 'Teste',
      })
    })
  })
})
