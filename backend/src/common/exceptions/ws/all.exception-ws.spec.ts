import { Test, TestingModule } from '@nestjs/testing'

import { AllExceptionWs } from './all.exception-ws'
import { ErrorType } from '../../enums/error-type.enum'
import { HttpStatus } from '@nestjs/common'
jest.mock('../../constants/exceptions', () => {
  return {
    COMMON_ERRORS: [
      {
        errorId: -1,
        message: 'Catioro',
        httpStatus: 403,
      },
    ],
  }
})
describe('Validation all exception', () => {
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AllExceptionWs],
    }).compile()
    module.init()
  })

  it('should be defined', () => {
    const allException = new AllExceptionWs(ErrorType.DOCUMENT_NOT_FOUND)
    expect(allException).toBeDefined()
  })

  it('should be return correct common', () => {
    const exCommonErrors = {
      errorId: -1,
      message: 'Catioro',
      httpStatus: HttpStatus.FORBIDDEN,
    }
    const allException = new AllExceptionWs(-1)
    expect(allException.tipoErroDado).toStrictEqual(exCommonErrors)
  })

  it('should be return correct new error', () => {
    const exCommonErrors = {
      errorId: 2,
      message: 'Unknown error',
      httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
    }
    const allException = new AllExceptionWs(2)
    expect(allException.tipoErroDado).toStrictEqual(exCommonErrors)
  })

  it('should be return just error and message', () => {
    const exCommonErrors = {
      errorId: 1,
      message: 'Error not cataloged',
      httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
    }
    const allException = new AllExceptionWs(1, 'Error not cataloged')
    expect(allException.tipoErroDado).toStrictEqual(exCommonErrors)
  })
})
