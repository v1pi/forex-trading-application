import { ErrorType } from '../../enums/error-type.enum'

export interface ErrorTypeDados {
  errorId: ErrorType
  message: string
  httpStatus?: number
}
