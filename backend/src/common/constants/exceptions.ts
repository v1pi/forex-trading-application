import { ErrorTypeDados } from '../exceptions/interfaces/tipo-erro-dados.interface'
import { HttpStatus } from '@nestjs/common'
import { ErrorType } from '../enums/error-type.enum'

export const COMMON_ERRORS: ErrorTypeDados[] = [
  {
    errorId: ErrorType.USER_NO_PERMISSION,
    message: 'You are not allowed to perform this action.',
    httpStatus: HttpStatus.FORBIDDEN,
  },
  {
    errorId: ErrorType.NO_AUTHENTICATED,
    message: 'You are not authenticated.',
    httpStatus: HttpStatus.UNAUTHORIZED,
  },
  {
    errorId: ErrorType.DOCUMENT_NOT_FOUND,
    message: 'Document not found.',
    httpStatus: HttpStatus.NOT_FOUND,
  },
  {
    errorId: ErrorType.ERROR_DB_CONNECTION,
    message: 'Error communicating with the database.',
    httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
  },
  {
    errorId: ErrorType.UNAVAILABLE_SERVICE,
    message: 'Unavailable service.',
    httpStatus: HttpStatus.SERVICE_UNAVAILABLE,
  },
  {
    errorId: ErrorType.COMMUNICATION_ERROR,
    message: 'Communication error between the Client and the Server.',
    httpStatus: HttpStatus.INTERNAL_SERVER_ERROR,
  },
  {
    errorId: ErrorType.ID_NOT_FOUND,
    message: 'Id not found.',
    httpStatus: HttpStatus.BAD_REQUEST,
  },
  {
    errorId: ErrorType.SAVING_ERROR,
    message: 'There was an error saving.',
    httpStatus: HttpStatus.BAD_REQUEST,
  },
  {
    errorId: ErrorType.UPDATING_ERROR,
    message: 'Error updating.',
    httpStatus: HttpStatus.BAD_REQUEST,
  },
  {
    errorId: ErrorType.DELETING_ERROR,
    message: 'Error while deleting.',
    httpStatus: HttpStatus.BAD_REQUEST,
  },
  {
    errorId: ErrorType.INVALID_DATA,
    message: 'Invalid data.',
    httpStatus: HttpStatus.BAD_REQUEST,
  },
]
