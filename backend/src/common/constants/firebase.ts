import * as admin from 'firebase-admin'
export const FIREBASE_CONFIG: admin.ServiceAccount = {
  projectId: 'forex-trading-application',
  privateKey:
    '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC3ZjGS6bH0L4ED\nWdZXSfCq58PUXUl0TDwAe9NtvBfYOiNegzh2Ya34zvTAkWMMZccc+qTPoXy+xWpv\nY2N5BB0vd7/6Dr80k6WMFjhDx027XoTm2+ZS5OmAIJ+A1A7Boj+xtm6I83eg/pRp\ndXFq/csvzOF3MPyJSoSLtOyDzi3K7HmxD7oSdsSGCcSXPbK+4tu3NZ4vzqRwJUHz\nVn2Vpo9n239nn5FphztCGxXFNvpnIKjr6QrEHE5O5e7AZYYWXN/82TDV+0lknxbH\nF0T7I2Yj2gYdeKdIsf8ShivtGAoJadblnV3afWjERmXgsFDmjUiLcv3eR82rQfbs\ne/R9RpyPAgMBAAECggEAQbPQRcAIGyFbmE46bhoUqL2QGAf1lT7sbgXuaE0uw62M\nV0MuWqbC0zSdVVGgiOP7ccPPgHzrelKyP0Z9b/DL3eIKOjTHRjgw4h9ht9JyZBJI\nDYaxHHhauKa3Tclqoe8U338qWVDTxi7+D0vePoS+opjFG1TG3A6ArqY55zxZhm0X\nGzB/zwPt9fe7vAutNVD1sTuv/6QjlWuBE/ycTryUxhkPX+yEMg2YRPTahVTuWPmb\ndl1yKkB4i0tuTLCQ78tUBDGEoF8pp00JpC6fbT0wUlZWf12pnqeUFYgNP42cw2ly\nm44dIn4F28Hiah4m3nrp/wjqFmCKkSLsadp7FyUFaQKBgQDeczfC4w1tWnDeTmbm\nkkzHDGRJsoIwU3tMvUt235cdiGxHXHE4HhrU2zvkHni7UDshisnZ/lpqjcrKGe5Y\nt6XJBe/df9GocIc8XHzxVrYlDfozoDVjw+t/lmcL690/S/M8PxiorbOsImAvjFJI\n8BJFxzn5ceBe/hPv0XAqtN6vpwKBgQDTDzliPIw3ReDVc6iO386Is4DzoRaTkto4\n/k6rPLXDPSh4pGZHCSoipt0bLVDZRzX+GRaXu6MqYoizkVopULvg4efAfl81bZhk\nDNFy2vcc944czO79ct6ZB+B8fOke3zfw4amaeIzILR/lU6WeaL3bBJwCeVb5VeWK\nRdlopZWI2QKBgQCp+Ke2xf7Qe7aaZpXBwC/iN9m5ZiiL/H3OGSW8YwP4kPELZdss\n4fPmiM2udIoTti44Zu/tHajAmi9FVaMFLu4COAMb4Dq21aS/vnvJfFQY5CE1qUii\nPmWHS8jZFxQq9UGZ2yxgVrle4bVVH4cQLEAKCTyHGTgz3TgmmoV2XFV7KQKBgQDA\n3FKFb+p4v4PZyNwn4l8qD00wUuz8YN7D4a4lT06QX5xR0XgcRUWZcHzuzpPoV+gA\njxBUCoPowS1FWPbYdguYTRWmklOR0NnF4uipWWkhsRzNOH7SXlsF32rElqe3LCZE\nGREQNEQAzyssNSlL/U4viqC8qgBNyq37kiQ+OcDQ0QKBgQClVBa4q3g4v64Xsaf1\n2t76S8EBplj/z7kNcZIljIOTV9gZlVyl7oIuTcT3gRO4TF4PuSpAKsPRQc09mYaX\n/zY9RYgBAD7I0MVY+KS3OcBdRkSRs+LOMiNPqOqCFm47LUOb9wp24UuCMSEhpv+V\nvdPLeWIRoCgrQt1gvFM9Yf0sMA==\n-----END PRIVATE KEY-----\n',
  clientEmail:
    'firebase-adminsdk-mvv23@forex-trading-application.iam.gserviceaccount.com',
}
