import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common'
import { User } from '../common/decorators/user.decorator'
import { ErrorType } from '../common/enums/error-type.enum'
import { ResponseDefault } from '../common/interfaces/response-default.interface'
import { OpenTradeDto } from './dto/open-trade.dto'
import { TradesService } from './trades.service'
import * as admin from 'firebase-admin'
import { ForexService } from '../forex/forex.service'
import { AllException } from '../common/exceptions/all.exception'
import { WalletsService } from 'src/wallets/wallets.service'
import * as moment from 'moment-timezone'
import { TradeStatus, TradeType } from 'src/models/trades/trades.interface'

@Controller('trades')
export class TradesController {
  constructor(
    private serv: TradesService,
    private forexServ: ForexService,
    private walletServ: WalletsService,
  ) {}
  @Post('open')
  public async openTrade(
    @Body() trade: OpenTradeDto,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    const wallet = await this.walletServ.getByUID(user.uid)
    if (trade.amount <= 0) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        'You can not open a trade with negative or zero amount.',
      )
    }
    if (wallet.amount < trade.amount) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        "You don't have enough money to do this trade.",
      )
    }
    delete trade.dateClosed
    delete trade.closed
    delete trade.profit
    trade.uid = user.uid
    await this.walletServ.updateAmount(trade.uid, -trade.amount)
    const job = await this.serv.open(trade)
    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        job,
      },
    }
  }

  @Put(':id/close')
  public async closeTrade(
    @Param('id') id: number,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    const trade = await this.serv.getByID(id)
    if (user.uid !== trade.uid) {
      throw new AllException(ErrorType.USER_NO_PERMISSION)
    } else if (trade.status === TradeStatus.CLOSE) {
      throw new AllException(
        ErrorType.INVALID_DATA,
        'This trade has already been closed.',
      )
    }
    const job = await this.serv.close(trade)
    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        job,
      },
    }
  }

  @Get(':id/info')
  public async getTradeById(
    @Param('id') id: number,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    const trade = await this.serv.getByID(id)
    if (user.uid !== trade.uid) {
      throw new AllException(ErrorType.USER_NO_PERMISSION)
    }

    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        trade,
      },
    }
  }

  @Get(':jobId/job-info')
  public async getTradeByJobId(
    @Param('jobId') id: string,
    @User() user: admin.auth.UserRecord,
  ): Promise<ResponseDefault> {
    try {
      const trade = await this.serv.getByJobID(id)
      if (user.uid !== trade.uid) {
        throw new AllException(ErrorType.USER_NO_PERMISSION)
      }

      return {
        error_id: ErrorType.NO_ERRORS,
        message: 'Success!',
        error: false,
        data: {
          trade,
        },
      }
    } catch (error) {
      if (error instanceof AllException) {
        return {
          error_id: ErrorType.NO_ERRORS,
          message: 'Success!',
          error: false,
          data: {
            job: id,
          },
        }
      } else {
        throw error
      }
    }
  }

  @Get(':uid/all')
  public async getAllTrade(
    @Param('uid') uid: string,
  ): Promise<ResponseDefault> {
    const trades = await this.serv.getAll(uid)

    return {
      error_id: ErrorType.NO_ERRORS,
      message: 'Success!',
      error: false,
      data: {
        trades,
      },
    }
  }
}
