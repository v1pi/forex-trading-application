import { InjectQueue } from '@nestjs/bull'
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import Bull, { Queue } from 'bull'
import { Trade } from 'src/models/trades/trades.entity'
import { DeepPartial, Repository } from 'typeorm'
import { ErrorType } from '../common/enums/error-type.enum'
import { AllException } from '../common/exceptions/all.exception'

@Injectable()
export class TradesService {
  constructor(
    @InjectRepository(Trade) private tradeDoc: Repository<Trade>,
    @InjectQueue('trades') private tradeQueue: Queue,
  ) {}
  public async create(trade: DeepPartial<Trade>): Promise<Trade> {
    return this.tradeDoc.save(trade)
  }

  public async open(trade: DeepPartial<Trade>): Promise<Bull.JobId> {
    const job = await this.tradeQueue.add('open', trade)
    return job.id
  }

  public async close(trade: DeepPartial<Trade>): Promise<Bull.JobId> {
    const job = await this.tradeQueue.add('close', trade)
    return job.id
  }

  public async update(id: number, trade: Trade): Promise<Trade> {
    const updated = await this.tradeDoc.update({ id: id }, trade)
    if (!updated.affected) {
      throw new AllException(ErrorType.UPDATING_ERROR)
    }
    return this.tradeDoc.findOne({ id: id })
  }

  public async getByID(id: number): Promise<Trade> {
    return this.tradeDoc.findOne({ id })
  }

  public async getByJobID(id: string): Promise<Trade> {
    const job = await this.tradeQueue.getJob(id)
    if (!(await job.isCompleted())) {
      throw new AllException(ErrorType.INVALID_DATA)
    }

    return job.returnvalue
  }

  public async getAll(uid: string): Promise<Trade[]> {
    return this.tradeDoc.find({ uid })
  }
}
