import { BullModule } from '@nestjs/bull'
import { HttpModule, Module } from '@nestjs/common'
import { ApiForexConfigModule } from 'src/config/api-forex/config.module'
import { ForexService } from 'src/forex/forex.service'
import { ForexEntityModule } from 'src/models/forex/forex.module'
import { TradesEntityModule } from 'src/models/trades/trades.module'
import { WalletsEntityModule } from 'src/models/wallets/wallets.module'
import { WalletsService } from 'src/wallets/wallets.service'
import { TradesController } from './trades.controller'
import { TradesProcessor } from './trades.processor'
import { TradesService } from './trades.service'

@Module({
  imports: [
    TradesEntityModule,
    ForexEntityModule,
    WalletsEntityModule,
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    ApiForexConfigModule,
    BullModule.registerQueue({
      name: 'trades',
    }),
  ],
  controllers: [TradesController],
  providers: [TradesService, ForexService, WalletsService, TradesProcessor],
})
export class TradesModule {}
