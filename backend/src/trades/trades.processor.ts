import { OnQueueFailed, Process, Processor } from '@nestjs/bull'
import { Logger } from '@nestjs/common'
import { Job } from 'bull'
import { TradeStatus, TradeType } from 'src/models/trades/trades.interface'
import { WalletsService } from 'src/wallets/wallets.service'
import { TradesService } from './trades.service'
import * as moment from 'moment-timezone'
import { ForexService } from 'src/forex/forex.service'

@Processor('trades')
export class TradesProcessor {
  private readonly logger = new Logger(TradesProcessor.name)

  constructor(
    private serv: TradesService,
    private forexServ: ForexService,
    private walletServ: WalletsService,
  ) {}

  @Process('open')
  async handleOpenTrade(job: Job) {
    const trade = job.data
    trade.status = TradeStatus.OPEN

    trade.dateOpen = moment().utc().toDate()
    const forex = (await this.forexServ.getLastForexValue())[0]
    trade.open = forex.close

    const newTrade = await this.serv.create(trade)
    return newTrade
  }

  @OnQueueFailed({ name: 'open' })
  async handleTrades(job: Job, err: Error) {
    await this.walletServ.updateAmount(job.data.uid, job.data.amount)
  }

  @Process('close')
  async handleCloseTrade(job: Job) {
    const trade = job.data
    trade.status = TradeStatus.CLOSE
    trade.dateClosed = moment().utc().toDate()
    const forex = (await this.forexServ.getLastForexValue())[0]
    trade.closed = forex.close

    const profit =
      (trade.type === TradeType.BUY ? 1 : -1) *
      (trade.closed - trade.open) *
      trade.amount *
      trade.multiplier
    trade.profit = profit
    await this.walletServ.updateAmount(trade.uid, profit + trade.amount)
    const newTrade = await this.serv.update(trade.id, trade)
    return newTrade
  }
}
