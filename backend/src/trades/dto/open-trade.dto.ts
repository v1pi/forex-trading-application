import { IsNumber, IsOptional } from 'class-validator'
import { TradeStatus, TradeType } from 'src/models/trades/trades.interface'

export class OpenTradeDto {
  @IsNumber()
  amount: number
  @IsNumber()
  multiplier: number
  @IsNumber()
  type: TradeType
  @IsOptional()
  uid: string
  @IsOptional()
  status: TradeStatus
  @IsOptional()
  open: number
  @IsOptional()
  closed: number
  @IsOptional()
  dateOpen: Date
  @IsOptional()
  dateClosed: Date
  @IsOptional()
  profit: number
}
