
CREATE SEQUENCE public.wallets_id_seq;

CREATE TABLE public.wallets (
                id INTEGER NOT NULL DEFAULT nextval('public.wallets_id_seq'),
                email VARCHAR NOT NULL,
                name VARCHAR NOT NULL,
                amount DOUBLE PRECISION NOT NULL,
                uid VARCHAR NOT NULL,
                CONSTRAINT wallets_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.wallets_id_seq OWNED BY public.wallets.id;

CREATE SEQUENCE public.trades_id_seq;

CREATE TABLE public.trades (
                id INTEGER NOT NULL DEFAULT nextval('public.trades_id_seq'),
                amount DOUBLE PRECISION NOT NULL,
                profit DOUBLE PRECISION,
                uid VARCHAR NOT NULL,
                multiplier INTEGER NOT NULL,
                type INTEGER NOT NULL,
                status INTEGER NOT NULL,
                open DOUBLE PRECISION NOT NULL,
                closed DOUBLE PRECISION,
                date_open TIMESTAMP NOT NULL,
                date_closed TIMESTAMP,
                CONSTRAINT trades_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.trades_id_seq OWNED BY public.trades.id;

CREATE SEQUENCE public.forex_id_seq;

CREATE TABLE public.forex (
                id INTEGER NOT NULL DEFAULT nextval('public.forex_id_seq'),
                close DOUBLE PRECISION NOT NULL,
                high DOUBLE PRECISION NOT NULL,
                open DOUBLE PRECISION NOT NULL,
                low DOUBLE PRECISION NOT NULL,
                date_time TIMESTAMP NOT NULL,
                CONSTRAINT forex_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.forex_id_seq OWNED BY public.forex.id;