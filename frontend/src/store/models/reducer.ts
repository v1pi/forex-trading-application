import { combineReducers } from 'redux'
import wallets from './wallets'

export default combineReducers({
  wallets: wallets,
})
