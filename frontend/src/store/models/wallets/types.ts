import { Wallet } from '../../../models/wallet.entity'

export enum WalletsTypes {
  CHANGE_SELECTED_WALLET = '@wallets/CHANGE_SELECTED',
}

export interface WalletsState {
  readonly selected: Wallet | null
}

export interface ChangeSelectedWallet {
  type: typeof WalletsTypes.CHANGE_SELECTED_WALLET
}
