import { HYDRATE } from 'next-redux-wrapper'
import { Reducer } from 'redux'
import { WalletsState, WalletsTypes } from './types'

const initialState: WalletsState = {
  selected: null,
}

const reducer: Reducer<WalletsState> = (state = initialState, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload.wallets }
    case WalletsTypes.CHANGE_SELECTED_WALLET:
      return { ...state, selected: action.payload }

    default:
      return state
  }
}

export default reducer
