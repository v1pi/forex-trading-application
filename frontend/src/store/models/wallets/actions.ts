import { action } from 'typesafe-actions'
import { Wallet } from '../../../models/wallet.entity'
import { WalletsTypes, ChangeSelectedWallet } from './types'

export const changeSelectedWallet = (
  newWallet: Wallet | null,
): ChangeSelectedWallet => {
  return action(WalletsTypes.CHANGE_SELECTED_WALLET, newWallet)
}
