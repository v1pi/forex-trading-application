import { createStore, Store, combineReducers } from 'redux'
import modelsReducer from './models/reducer'
import { WalletsState } from './models/wallets/types'
import { createWrapper } from 'next-redux-wrapper'

export interface ApplicationState {
  models: ModelsState
}

export interface ModelsState {
  wallets: WalletsState
}
const combinedReducers = combineReducers({
  models: modelsReducer,
})
const makeStore = () => {
  const store = createStore(combinedReducers)
  return store
}
const store = createWrapper(makeStore, { debug: false })
export { store }
