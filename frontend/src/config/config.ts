const API_URL = process.env.REACT_APP_API_URL || 'http://localhost'
const API_PORT = process.env.REACT_APP_API_PORT || '3001'
const WS_URL = process.env.REACT_APP_WS_URL || 'ws://localhost'
const WS_PORT = process.env.REACT_APP_WS_PORT || '3001'

export { API_URL, API_PORT, WS_URL, WS_PORT }
