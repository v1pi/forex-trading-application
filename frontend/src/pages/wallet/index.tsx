import React, { Component } from 'react'
import { NavBar } from '../../components/NavBar'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import swal from '@sweetalert/with-react'
import { TradeController } from '../../controllers/trade.controller'
import { ApplicationState } from '../../store'
import { bindActionCreators, Dispatch } from 'redux'
import * as WalletActions from '../../store/models/wallets/actions'
import { connect } from 'react-redux'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { FirebaseHelper } from '../../helpers/firebase.helper'
import { WalletController } from '../../controllers/wallet.controller'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLevelDownAlt, faLevelUpAlt } from '@fortawesome/free-solid-svg-icons'
import { NextRouter, withRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
interface PropsWithRouter {
  router: NextRouter
}
type Props = typeof WalletActions &
  ApplicationState &
  PropsWithRouter &
  UseTranslationResponse<'common'>
interface HomeTradeState {
  withdraw: number
  deposit: number
  amount: number
}

class WalletHomePage extends Component<Props, HomeTradeState> {
  constructor(props: Props) {
    super(props)
    this.loadWallet = this.loadWallet.bind(this)
    this.deposit = this.deposit.bind(this)
    this.withdraw = this.withdraw.bind(this)
    this.state = {
      withdraw: 0,
      deposit: 0,
      amount: 0,
    }
  }

  render(): JSX.Element {
    return (
      <div>
        <NavBar {...this.props}></NavBar>
        <div className="center-text">
          <br></br>
          <h3>{this.props.t('title')}</h3>
        </div>
        <Container>
          <div style={{ marginBottom: '20px' }}>
            <strong>
              {this.props.t('money')} USD{' '}
              {this.props.models.wallets.selected?.amount.toFixed(2)}
            </strong>
          </div>
          <div className="wallet-form">
            <div>
              <Form.Group controlId="formBasicAmount">
                <Form.Control
                  type="number"
                  placeholder={this.props.t('amount')}
                  onChange={(event) =>
                    this.setState({ amount: Number(event.target.value) })
                  }
                />
              </Form.Group>
            </div>
            <hr></hr>
            <div>
              <Button onClick={this.deposit} className="btn-black">
                <FontAwesomeIcon icon={faLevelDownAlt}></FontAwesomeIcon>
                {'  '}
                {this.props.t('deposit')}
              </Button>
            </div>
            <div>
              <Button onClick={this.withdraw} className="btn-white">
                <FontAwesomeIcon icon={faLevelUpAlt}></FontAwesomeIcon>
                {'  '}
                {this.props.t('withdraw')}
              </Button>
            </div>
          </div>
        </Container>
      </div>
    )
  }

  async deposit(): Promise<void> {
    try {
      if (this.state.amount <= 0) {
        throw new Error(this.props.t('common:error-amount'))
      }
      const walletController = new WalletController()
      const newWallet = await walletController.deposit(this.state.amount)
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text-2'),
        icon: 'success',
        buttons: {
          ok: { text: this.props.t('swal-success-btn') },
        },
      })
      this.props.changeSelectedWallet(newWallet)
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  async withdraw(): Promise<void> {
    try {
      if (this.state.amount <= 0) {
        throw new Error(this.props.t('common:error-amount'))
      }
      const walletController = new WalletController()
      const newWallet = await walletController.withdraw(this.state.amount)
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text-1'),
        icon: 'success',
        buttons: {
          ok: { text: this.props.t('swal-success-btn') },
        },
      })
      this.props.changeSelectedWallet(newWallet)
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  async loadWallet(): Promise<void> {
    const walletController = new WalletController()

    const wallet = await walletController.getWallet()
    this.props.changeSelectedWallet(wallet)
  }

  async initPage(): Promise<void> {
    if (FirebaseHelper.Instance.auth.currentUser) {
      this.loadWallet()
    } else {
      await new Promise(() =>
        setTimeout(() => {
          if (FirebaseHelper.Instance.auth.currentUser) {
            this.loadWallet()
          } else {
            this.props.router.replace('/', undefined, {
              locale: this.props.i18n.language,
            })
          }
        }, 1000),
      )
    }
  }

  componentDidMount(): void {
    this.initPage()
  }
}

const mapStateToProps = (state: ApplicationState): ApplicationState => state
const mapDispatchToProps = (dispatch: Dispatch): typeof WalletActions =>
  bindActionCreators({ ...WalletActions }, dispatch)

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['wallet', 'common'])),
  },
})
const WalletPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation(['wallet', 'common'])(withRouter(WalletHomePage)))

export default WalletPage
