import React, { Component } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { NavBar } from '../../components/NavBar'
import { FirebaseHelper } from '../../helpers/firebase.helper'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import swal from '@sweetalert/with-react'
import { SocketHelper } from '../../helpers/socket.helper'
import Chart from 'react-apexcharts'
import { ForexHelper } from '../../helpers/forex.helper'
import { Candlestick } from '../../components/Candlestick'
import { NextRouter, withRouter } from 'next/router'
import { Forex } from '../../models/forex.entity'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
interface HomeForexState {
  listForex: Forex[]
}

interface PropsWithProps {
  router: NextRouter
}

type Props = PropsWithProps & UseTranslationResponse<'common'>

class ForexPage extends Component<Props, HomeForexState> {
  constructor(props: Props) {
    super(props)
    this.connectSocket = this.connectSocket.bind(this)
    this.initPage = this.initPage.bind(this)
    this.state = {
      listForex: [],
    }
  }

  connectSocket(): void {
    const socket = SocketHelper.Instance.socket
    if (socket.connected) {
      socket.emit('forex')
    }
    socket.on('connect', () => {
      console.log('Connected')
      socket.emit('forex')
    })

    socket.on('forex', (data: any) => {
      const newList: Forex[] = [...this.state.listForex]
      for (const forex of data) {
        const newForex = Forex.fromJson(forex)
        if (!newList.some((f) => f.id === newForex.id)) {
          newList.push(newForex)
        }
      }
      this.setState({
        listForex: newList,
      })
    })
  }

  render(): JSX.Element {
    return (
      <div>
        <NavBar {...this.props}></NavBar>
        <div className="center-text">
          <br></br>
          <h3>{this.props.t('h1')}</h3>
          <Container>
            <Candlestick
              series={ForexHelper.Instance.convertToSeries(
                this.state.listForex,
              )}
              minY={ForexHelper.Instance.getMinLimitY(this.state.listForex)}
              maxY={ForexHelper.Instance.getMaxLimitY(this.state.listForex)}
              height="500"
              width="100%"
            ></Candlestick>
          </Container>
        </div>
      </div>
    )
  }

  async initPage(): Promise<void> {
    if (FirebaseHelper.Instance.auth.currentUser) {
      this.connectSocket()
    } else {
      await new Promise(() =>
        setTimeout(() => {
          if (FirebaseHelper.Instance.auth.currentUser) {
            this.connectSocket()
          } else {
            this.props.router.replace('/', undefined, {
              locale: this.props.i18n.language,
            })
          }
        }, 1000),
      )
    }
  }

  componentDidMount(): void {
    this.initPage()
  }
}
export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['forex', 'common'])),
    },
  }
}
export default withTranslation(['forex', 'common'])(withRouter(ForexPage))
