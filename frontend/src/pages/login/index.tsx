import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { NavBar } from '../../components/NavBar'
import { FirebaseHelper } from '../../helpers/firebase.helper'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import swal from '@sweetalert/with-react'
import { NextRouter, withRouter } from 'next/router'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
interface LoginState {
  email: string
  password: string
}
interface PropsWithRouter {
  router: NextRouter
}
type Props = UseTranslationResponse<'common'> & PropsWithRouter
class LoginPage extends Component<Props, LoginState> {
  constructor(props: Props) {
    super(props)
    this.handleLogin = this.handleLogin.bind(this)
    this.initPage = this.initPage.bind(this)
    this.state = {
      email: '',
      password: '',
    }
  }

  async handleLogin(): Promise<void> {
    try {
      await FirebaseHelper.Instance.auth.signInWithEmailAndPassword(
        this.state.email,
        this.state.password,
      )
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text'),
        icon: 'success',
        buttons: {
          produtos: { text: this.props.t('swal-success-btn'), value: 'forex' },
        },
      }).then((value: string) => {
        this.props.router.replace('/forex', undefined, {
          locale: this.props.i18n.language,
        })
      })
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  render(): JSX.Element {
    return (
      <div>
        <NavBar {...this.props}></NavBar>
        <div className="center-content">
          <div className="center-text">
            <h3>{this.props.t('h1')}</h3>
            <p>{this.props.t('subtitle')}</p>
          </div>
          <Form className="form">
            <Form.Group controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Email"
                onChange={(event) =>
                  this.setState({ email: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicSenha">
              <Form.Control
                type="password"
                placeholder={this.props.t('common:password')}
                onChange={(event) =>
                  this.setState({ password: event.target.value })
                }
              />
            </Form.Group>
            <div className="center-text">
              <Button onClick={this.handleLogin}>Login</Button>
            </div>
          </Form>
        </div>
      </div>
    )
  }

  async initPage(): Promise<void> {
    await new Promise(() =>
      setTimeout(() => {
        if (
          FirebaseHelper.Instance.auth.currentUser &&
          (this.props.router.pathname === '/' ||
            this.props.router.pathname === '/login')
        ) {
          this.props.router.replace('/forex', undefined, {
            locale: this.props.i18n.language,
          })
        }
      }, 1000),
    )
  }

  componentDidMount(): void {
    this.initPage()
  }
}
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['login', 'common'])),
  },
})
export default withTranslation(['login', 'common'])(withRouter(LoginPage))
