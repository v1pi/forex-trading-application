import React, { Component } from 'react'
import {
  Button,
  Form,
  Col,
  Row,
  Container,
  DropdownButton,
  Dropdown,
} from 'react-bootstrap'
import { NavBar } from '../../components/NavBar'
import { FirebaseHelper } from '../../helpers/firebase.helper'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import swal from '@sweetalert/with-react'
import { Trade, TradeStatus, TradeType } from '../../models/trade.entity'
import { SocketHelper } from '../../helpers/socket.helper'
import { TradeController } from '../../controllers/trade.controller'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faArrowUp,
  faArrowDown,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons'
import { Forex } from '../../models/forex.entity'
import { Candlestick } from '../../components/Candlestick'
import { ForexHelper } from '../../helpers/forex.helper'
import { ApplicationState } from '../../store'
import { bindActionCreators, Dispatch } from 'redux'
import * as WalletActions from '../../store/models/wallets/actions'
import { connect } from 'react-redux'
import { WalletController } from '../../controllers/wallet.controller'
import { NextRouter, withRouter } from 'next/router'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
import dynamic from 'next/dynamic'
import moment from 'moment-timezone'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
interface PropsWithRouter {
  router: NextRouter
}
type Props = typeof WalletActions &
  PropsWithRouter &
  UseTranslationResponse<'common'> &
  ApplicationState
interface HomeTradeState {
  trades: Trade[]
  openChoosed: boolean
  listForex: Forex[]
  amount: number
  multiplier: number
  type: TradeType
}

class TradeHomePage extends Component<Props, HomeTradeState> {
  constructor(props: Props) {
    super(props)
    this.connectSocket = this.connectSocket.bind(this)
    this.getHistoryTrades = this.getHistoryTrades.bind(this)
    this.renderClosedGraph = this.renderClosedGraph.bind(this)
    this.renderTrades = this.renderTrades.bind(this)
    this.renderNewTradeForm = this.renderNewTradeForm.bind(this)
    this.handleDoTrade = this.handleDoTrade.bind(this)
    this.initPage = this.initPage.bind(this)
    this.handleCloseTrade = this.handleCloseTrade.bind(this)
    this.schedulerCheckJob = this.schedulerCheckJob.bind(this)
    this.state = {
      trades: [],
      openChoosed: true,
      listForex: [],
      amount: 0,
      multiplier: 1,
      type: TradeType.BUY,
    }
  }

  async getHistoryTrades(): Promise<void> {
    const tradeController = new TradeController()
    const trades = await tradeController.getAllTrades()
    this.setState({
      trades,
    })
  }

  connectSocket(): void {
    const socket = SocketHelper.Instance.socket
    if (socket.connected) {
      socket.emit('forex')
    }
    socket.on('connect', () => {
      console.log('Connected')
      socket.emit('forex')
    })

    socket.on('forex', (data: any) => {
      const newList: Forex[] = [...this.state.listForex]
      for (const forex of data) {
        const newForex = Forex.fromJson(forex)
        if (!newList.some((f) => f.id === newForex.id)) {
          newList.push(newForex)
        }
      }
      this.setState({
        listForex: newList,
      })
    })
  }

  renderNewTradeForm(): JSX.Element {
    return (
      <Container className="new-trade">
        <p>{this.props.t('trade-title')}</p>
        <div className="trade-form">
          <div>
            <label htmlFor="type">{this.props.t('trade-type')}</label>
            <select
              name="type"
              id="type"
              onChange={(event) =>
                this.setState({ type: Number(event.currentTarget.value) })
              }
            >
              <option value="1">{this.props.t('buy')}</option>
              <option value="0">{this.props.t('sell')}</option>
            </select>
          </div>

          <div>
            <label htmlFor="multiplier">
              {this.props.t('trade-multiplier')}
            </label>
            <select
              name="multiplier"
              id="multiplier"
              onChange={(event) =>
                this.setState({ multiplier: Number(event.currentTarget.value) })
              }
            >
              <option value="1">1</option>
              <option value="20">20</option>
              <option value="50">50</option>
            </select>
          </div>
          <hr></hr>
          <div>
            <Form.Group controlId="formBasicAmount">
              <Form.Control
                type="number"
                placeholder={this.props.t('trade-amount')}
                onChange={(event) =>
                  this.setState({ amount: Number(event.target.value) })
                }
              />
            </Form.Group>
          </div>
          <div>
            <Button onClick={this.handleDoTrade} className="btn-black">
              {this.props.t('trade-open')}
            </Button>
          </div>
        </div>
      </Container>
    )
  }

  async handleDoTrade(): Promise<void> {
    const trade = new Trade()

    trade.amount = this.state.amount
    trade.type = this.state.type
    trade.dateOpen = moment()
    trade.multiplier = this.state.multiplier
    trade.status = TradeStatus.OPEN
    try {
      if (this.state.amount <= 0) {
        throw new Error(this.props.t('common:error-amount'))
      }
      if (this.state.amount > this.props.models.wallets.selected.amount) {
        throw new Error(this.props.t('error-enough-money'))
      }
      const tradeController = new TradeController()
      const walletController = new WalletController()
      const jobId = await tradeController.open(trade)
      trade.jobId = jobId
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text-1'),
        icon: 'success',
        buttons: {
          ok: { text: 'Ok' },
        },
      })
      const newWallet = await walletController.getWallet()
      this.props.changeSelectedWallet(newWallet)
      const trades = this.state.trades.filter((el) => el.jobId !== trade.jobId)
      trades.unshift(trade)
      this.setState({
        trades,
      })
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  render(): JSX.Element {
    return (
      <div>
        <NavBar {...this.props}></NavBar>
        <div className="center-text">
          <br></br>
          <h3>{this.props.t('h1')}</h3>
        </div>
        <Container className="center-container candlestick">
          <div>
            <Candlestick
              series={ForexHelper.Instance.convertToSeries(
                this.state.listForex,
              )}
              minY={ForexHelper.Instance.getMinLimitY(this.state.listForex)}
              maxY={ForexHelper.Instance.getMaxLimitY(this.state.listForex)}
              height="300"
              width="100%"
            ></Candlestick>
          </div>
        </Container>
        {this.renderNewTradeForm()}
        <div className="center-text ">
          <h3>{this.props.t('history')}</h3>
        </div>
        <Container className="trade-history">
          <Row className="trade-options">
            <Col
              className={this.state.openChoosed ? 'choosed' : ''}
              onClick={() => this.setState({ openChoosed: true })}
            >
              {this.props.t('open')}
            </Col>
            <div className="line-beteween"></div>
            <Col
              className={!this.state.openChoosed ? 'choosed' : ''}
              onClick={() => this.setState({ openChoosed: false })}
            >
              {this.props.t('closed')}
            </Col>
          </Row>
          {this.renderTrades()}
        </Container>
        {this.renderClosedGraph()}
      </div>
    )
  }

  async schedulerCheckJob(trade: Trade, nError = 0): Promise<void> {
    if (nError <= 3) {
      try {
        const tradeController = new TradeController()
        const newTrade = await tradeController.getTradeByJobId(trade.jobId)
        const trades = this.state.trades.filter(
          (el) => el.jobId !== trade.jobId,
        )
        trades.unshift(newTrade)
        this.setState({
          trades,
        })
      } catch (error) {
        setTimeout(
          this.schedulerCheckJob,
          1000 * Math.pow(2, nError + 1),
          trade,
          nError + 1,
        )
      }
    } else {
      /* const trades = this.state.trades.filter((el) => el.jobId !== trade.jobId)
      this.setState({
        trades,
      }) */
      swal({
        title: this.props.t('common:error'),
        text: this.props.t('swal-error-text'),
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  renderTrades(): JSX.Element[] {
    const tradeType: TradeStatus = this.state.openChoosed
      ? TradeStatus.OPEN
      : TradeStatus.CLOSE
    const filteredTrades = this.state.trades.filter(
      (el) => el.status === tradeType,
    )
    return filteredTrades.length === 0
      ? [
          <Container className="no-data" key={0}>
            {this.props.t('no-data')}
          </Container>,
        ]
      : filteredTrades.map<JSX.Element>((trade, idx) => {
          const lastTradex =
            this.state.listForex.length > 0
              ? this.state.listForex[this.state.listForex.length - 1]
              : new Forex()

          if (trade.jobId) {
            setTimeout(this.schedulerCheckJob, 1000, trade)
          }

          const amountToShow =
            trade.status === TradeStatus.OPEN
              ? ((lastTradex.close ?? trade.open) - trade.open) *
                (trade.type === TradeType.BUY ? 1 : -1) *
                trade.amount *
                trade.multiplier
              : trade.profit
          return (
            <Container key={idx} className="trade-item">
              <Row
                className={
                  trade.status === TradeStatus.CLOSE || trade.jobId
                    ? 'closed'
                    : ''
                }
              >
                <Col className="trade-currency">
                  <FontAwesomeIcon
                    icon={
                      trade.type === TradeType.BUY ? faArrowUp : faArrowDown
                    }
                  />
                  <span>GBP/USD</span>
                </Col>

                <Col className="trade-info">
                  <Row>
                    <span>
                      {!trade.jobId ? (
                        <span
                          className={
                            amountToShow > 0
                              ? 'positive-amount'
                              : 'negative-amount'
                          }
                        >
                          {amountToShow > 0 ? '+' : ''}
                          {amountToShow.toFixed(2)} USD
                        </span>
                      ) : (
                        <span className={'pending'}>
                          {this.props.t('pending')}
                        </span>
                      )}

                      {'  '}
                      <FontAwesomeIcon icon={faAngleRight} />
                    </span>
                  </Row>
                  <Row style={{ color: 'gray' }}>
                    {`${trade.amount.toFixed()}, x${
                      trade.multiplier
                    }, ${(trade.status === TradeStatus.OPEN
                      ? trade.dateOpen
                      : trade.dateClosed
                    ).format('YYYY-MM-DD HH:mm')}`}
                  </Row>
                </Col>
              </Row>
              <Row
                className={
                  trade.status === TradeStatus.CLOSE || trade.jobId
                    ? 'closed close-trade'
                    : 'close-trade'
                }
              >
                <Button
                  onClick={() => this.handleCloseTrade(trade)}
                  className="btn-close-trade btn-white"
                >
                  {this.props.t('trade-close')}
                </Button>
              </Row>
            </Container>
          )
        })
  }

  async handleCloseTrade(trade: Trade): Promise<void> {
    try {
      const tradeController = new TradeController()
      const walletController = new WalletController()
      const jobId = await tradeController.close(trade.id)
      trade.dateClosed = moment()
      trade.status = TradeStatus.CLOSE
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text-2'),
        icon: 'success',
        buttons: {
          produtos: { text: 'Ok' },
        },
      })
      const newWallet = await walletController.getWallet()
      this.props.changeSelectedWallet(newWallet)
      trade.jobId = jobId
      const trades = this.state.trades.filter((el) => el.id !== trade.id)
      trades.unshift(trade)
      this.setState({
        trades,
      })
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  renderClosedGraph(): JSX.Element {
    const filteredTrades = this.state.trades.filter(
      (trade) => TradeStatus.CLOSE === trade.status && trade.profit,
    )
    if (filteredTrades.length <= 1) {
      return <span></span>
    }
    return (
      <Container className="history-close candlestick">
        <div>
          <div className="center-text">
            <br></br>
            <h3>{this.props.t('earned')}</h3>
          </div>
          <Chart
            series={[
              {
                data: filteredTrades.map((trade) => ({
                  x: trade.dateClosed.toDate(),
                  y: trade.profit.toFixed(2),
                })),
              },
            ]}
            type="area"
            height="300"
            width="100%"
            options={{
              title: {
                text: 'USD',
                align: 'left',
              },
              xaxis: {
                type: 'datetime',
                labels: {
                  datetimeUTC: false,
                  datetimeFormatter: {
                    year: 'yyyy',
                    month: "MMM 'yy",
                    day: 'dd MMM',
                    hour: 'HH:mm',
                  },
                },
              },
              yaxis: {
                tickAmount: 2,
                tooltip: {
                  enabled: true,
                },
              },
            }}
          ></Chart>
        </div>
      </Container>
    )
  }

  async initPage(): Promise<void> {
    if (FirebaseHelper.Instance.auth.currentUser) {
      this.getHistoryTrades()
      this.connectSocket()
    } else {
      await new Promise(() =>
        setTimeout(() => {
          if (FirebaseHelper.Instance.auth.currentUser) {
            this.getHistoryTrades()
            this.connectSocket()
          } else {
            this.props.router.replace('/', undefined, {
              locale: this.props.i18n.language,
            })
          }
        }, 1000),
      )
    }
  }

  componentDidMount(): void {
    this.initPage()
  }
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['trades', 'common'])),
  },
})

const mapStateToProps = (state: ApplicationState): ApplicationState => state
const mapDispatchToProps = (dispatch: Dispatch): typeof WalletActions =>
  bindActionCreators({ ...WalletActions }, dispatch)
const TradePage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation(['trades', 'common'])(withRouter(TradeHomePage)))

export default TradePage
