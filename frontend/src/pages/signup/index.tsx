import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { NavBar } from '../../components/NavBar'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import swal from '@sweetalert/with-react'
import { SignUpController } from '../../controllers/signup.controller'
import { FirebaseHelper } from '../../helpers/firebase.helper'
import { NextRouter, withRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
interface HomeRegistrarState {
  email: string
  password: string
  name: string
}

interface PropsWithRouter {
  router: NextRouter
}

type Props = PropsWithRouter & UseTranslationResponse<'common'>
class SignUpPage extends Component<Props, HomeRegistrarState> {
  constructor(props: any) {
    super(props)

    this.handleRegister = this.handleRegister.bind(this)
    this.initPage = this.initPage.bind(this)
    this.state = {
      email: '',
      password: '',
      name: '',
    }
  }

  async handleRegister(): Promise<void> {
    const registerController = new SignUpController()
    try {
      if (this.state.name.length < 1) {
        throw Error(this.props.t('error-name'))
      }
      if (this.state.email.length < 1) {
        throw Error(this.props.t('error-email'))
      }
      if (this.state.password.length < 1) {
        throw Error(this.props.t('error-password'))
      }
      await registerController.signUp({
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
      })
      swal({
        title: this.props.t('common:successful'),
        text: this.props.t('swal-success-text'),
        icon: 'success',
        buttons: {
          produtos: { text: this.props.t('swal-success-btn'), value: 'login' },
        },
      }).then((value: string) => {
        if (value === 'login') {
          this.props.router.replace('/', undefined, {
            locale: this.props.i18n.language,
          })
        }
      })
    } catch (error) {
      swal({
        title: this.props.t('common:error'),
        text: error.message,
        icon: 'error',
        buttons: {
          cancel: this.props.t('common:close'),
        },
      })
    }
  }

  render(): JSX.Element {
    return (
      <div>
        <NavBar {...this.props}></NavBar>
        <div className="center-content">
          <div className="center-text">
            <h3>{this.props.t('h1')}</h3>
            <p>{this.props.t('subtitle')}</p>
          </div>
          <Form className="form">
            <Form.Group controlId="formBasicNome">
              <Form.Control
                type="text"
                placeholder={this.props.t('common:name')}
                onChange={(event) =>
                  this.setState({ name: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Control
                type="email"
                placeholder="Email"
                onChange={(event) =>
                  this.setState({ email: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicSenha">
              <Form.Control
                type="password"
                placeholder={this.props.t('common:password')}
                onChange={(event) =>
                  this.setState({ password: event.target.value })
                }
              />
            </Form.Group>
            <div className="center-text">
              <Button onClick={this.handleRegister}>
                {this.props.t('h1')}
              </Button>
            </div>
          </Form>
        </div>
      </div>
    )
  }

  async initPage(): Promise<void> {
    await new Promise(() =>
      setTimeout(() => {
        if (
          FirebaseHelper.Instance.auth.currentUser &&
          this.props.router.pathname === '/signup'
        ) {
          this.props.router.replace('/forex', undefined, {
            locale: this.props.i18n.language,
          })
        }
      }, 1000),
    )
  }

  componentDidMount(): void {
    this.initPage()
  }
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['signup', 'common'])),
  },
})
export default withTranslation(['signup', 'common'])(withRouter(SignUpPage))
