import React from 'react'
import { store } from '../store'
import 'bootstrap/dist/css/bootstrap.min.css'
import './styles/globals.css'
import './styles/navbar.css'

import './styles/login.css'
import './styles/forex.css'
import './styles/wallet.css'

import './styles/trade.css'
import { appWithTranslation } from 'next-i18next'
// eslint-disable-next-line react/prop-types
function App({ Component, pageProps }): JSX.Element {
  return <Component {...pageProps} />
}

export default store.withRedux(appWithTranslation(App))
