import React, { Component } from 'react'
import { Series } from '../helpers/forex.helper'
import { Trade } from '../models/trade.entity'
import dynamic from 'next/dynamic'
const Chart = dynamic(() => import('react-apexcharts'), { ssr: false })
interface CandlestickProps {
  series: Series[]
  minY: number
  maxY: number
  height: string
  width: string
}
export class Candlestick extends Component<CandlestickProps> {
  render(): JSX.Element {
    return (
      <Chart
        series={[
          {
            data: this.props.series,
          },
        ]}
        type="candlestick"
        height={this.props.height}
        width={this.props.width}
        options={{
          title: {
            text: 'GBPUSD',
            align: 'left',
          },
          xaxis: {
            type: 'datetime',
            labels: {
              datetimeUTC: false,
              datetimeFormatter: {
                year: 'yyyy',
                month: "MMM 'yy",
                day: 'dd MMM',
                hour: 'HH:mm',
              },
            },
          },
          yaxis: {
            type: 'numeric',
            tickAmount: 2,
            min: this.props.minY,
            max: this.props.maxY,
            tooltip: {
              enabled: true,
            },
          },
        }}
      />
    )
  }
}
