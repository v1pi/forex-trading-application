import React, { Component } from 'react'
import { Nav, Navbar } from 'react-bootstrap'
import { FirebaseHelper } from '../helpers/firebase.helper'
import { Wallet } from '../models/wallet.entity'
import { WalletController } from '../controllers/wallet.controller'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons'
import { ApplicationState, store } from '../store'
import { bindActionCreators, Dispatch } from 'redux'
import * as WalletActions from '../store/models/wallets/actions'
import { connect } from 'react-redux'
import { NextRouter, useRouter, withRouter } from 'next/router'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { withTranslation, UseTranslationResponse } from 'react-i18next'
import { I18n } from 'next-i18next'
interface WithRouterProps {
  router: NextRouter
  t: any
  i18n: I18n
  ready: boolean
}
interface NavBarState {
  hasUser: boolean
}
type Props = typeof WalletActions & ApplicationState & WithRouterProps
class NavBar extends Component<Props, NavBarState> {
  constructor(props: Props) {
    super(props)
    this.loadWallet = this.loadWallet.bind(this)
    this.renderSignout = this.renderSignout.bind(this)
    this.renderWallet = this.renderWallet.bind(this)
    this.renderNav = this.renderNav.bind(this)
    this.handleChooseLanguage = this.handleChooseLanguage.bind(this)
    this.renderChooseLanguage = this.renderChooseLanguage.bind(this)
    this.initPage = this.initPage.bind(this)
    this.state = {
      hasUser: !!FirebaseHelper.Instance.auth.currentUser,
    }
  }

  async loadWallet(): Promise<void> {
    const walletController = new WalletController()

    const wallet = await walletController.getWallet()
    this.props.changeSelectedWallet(wallet)
  }

  renderWallet(): JSX.Element {
    if (!this.state.hasUser) {
      return <span></span>
    }
    if (!this.props.models.wallets.selected) {
      this.loadWallet()
      return <span></span>
    }
    return (
      <Navbar.Text className="nav-wallet">
        <span>
          {' '}
          {this.props.t('common:hi')},{' '}
          {this.props.models.wallets.selected.name.split(' ')[0]}
        </span>
        <FontAwesomeIcon icon={faMoneyBill} />
        <span> USD {this.props.models.wallets.selected.amount.toFixed(2)}</span>
      </Navbar.Text>
    )
  }

  renderSignout(): JSX.Element {
    if (!this.state.hasUser) {
      return <span></span>
    }
    if (
      this.props.router.pathname === '/' ||
      this.props.router.pathname === '/signup'
    ) {
      this.props.router.replace(`/forex`, undefined, {
        locale: this.props.i18n.language,
      })
    }
    return (
      <Navbar.Text className="signout">
        <Nav.Link
          onClick={async () => {
            await FirebaseHelper.Instance.auth.signOut()
            this.setState({
              hasUser: false,
            })
            this.props.changeSelectedWallet(null)
            this.props.router.replace('/', undefined, {
              locale: this.props.i18n.language,
            })
          }}
        >
          {this.props.t('common:logout')}
        </Nav.Link>
      </Navbar.Text>
    )
  }

  async handleChooseLanguage(language: string): Promise<void> {
    if (language !== this.props.i18n.language) {
      await this.props.i18n.changeLanguage(language)
      if (language !== 'en' && !window.location.href.includes('/pt/')) {
        console.log(this.props.router.pathname)
        window.history.replaceState(
          null,
          'Dafiti - Module 1',
          language + this.props.router.pathname,
        )
        window.location.reload()
      }
    }
  }

  renderChooseLanguage(): JSX.Element {
    return (
      <Navbar.Text className="choose-language">
        <span
          className={
            this.props.i18n.language === 'pt' ? 'language-choosed' : ''
          }
          onClick={() => this.handleChooseLanguage('pt')}
        >
          BR
        </span>
        <span
          className={
            this.props.i18n.language === 'en' ? 'language-choosed' : ''
          }
          onClick={() => this.handleChooseLanguage('en')}
        >
          EN
        </span>
      </Navbar.Text>
    )
  }

  renderNav(): JSX.Element {
    return this.state.hasUser ? (
      <Nav className="mr-auto">
        <Nav.Link
          className={
            this.props.router.pathname === '/forex' ? 'nav-choosed' : ''
          }
          onClick={() =>
            this.props.router.push('/forex', undefined, {
              locale: this.props.i18n.language,
            })
          }
        >
          {this.props.t('common:forex')}
        </Nav.Link>
        <Nav.Link
          className={
            this.props.router.pathname === '/trades' ? 'nav-choosed' : ''
          }
          onClick={() =>
            this.props.router.push('/trades', undefined, {
              locale: this.props.i18n.language,
            })
          }
        >
          {this.props.t('common:trades')}
        </Nav.Link>
        <Nav.Link
          className={
            this.props.router.pathname === '/wallet' ? 'nav-choosed' : ''
          }
          onClick={() =>
            this.props.router.push('/wallet', undefined, {
              locale: this.props.i18n.language,
            })
          }
        >
          {this.props.t('common:wallet')}
        </Nav.Link>
      </Nav>
    ) : (
      <Nav className="mr-auto">
        <Nav.Link
          className={
            this.props.router.pathname === '/signup' ? 'nav-choosed' : ''
          }
          onClick={() => {
            this.props.router.push('/signup', undefined, {
              locale: this.props.i18n.language,
            })
          }}
        >
          {this.props.t('common:signup')}
        </Nav.Link>
      </Nav>
    )
  }

  render(): JSX.Element {
    return (
      <div>
        <Navbar collapseOnSelect bg="light" expand="sm">
          <Navbar.Brand
            onClick={() =>
              this.props.router.push('/', undefined, {
                locale: this.props.i18n.language,
              })
            }
            style={{ cursor: 'pointer' }}
          >
            <img
              src="dafiti.svg"
              width="75"
              height="75"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse
            id="responsive-navbar-nav"
            className="justify-content-end"
          >
            {this.renderNav()}
            <Nav className="nav-right">
              {this.renderWallet()}
              {this.renderChooseLanguage()}
              {this.renderSignout()}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    )
  }

  async initPage(): Promise<void> {
    await new Promise(() =>
      setTimeout(() => {
        if (FirebaseHelper.Instance.auth.currentUser) {
          this.setState({
            hasUser: true,
          })
        } else {
          this.setState({
            hasUser: false,
          })
        }
      }, 1000),
    )
  }

  componentDidMount() {
    this.initPage()
  }
}
const mapStateToProps = (state: ApplicationState): ApplicationState => state
const mapDispatchToProps = (dispatch: Dispatch): typeof WalletActions =>
  bindActionCreators({ ...WalletActions }, dispatch)
const navExport = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavBar)

export {navExport as NavBar}
