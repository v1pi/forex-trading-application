import firebase from 'firebase'
import 'firebase/auth'
import 'firebase/firestore'

interface FirebaseConfigs {
  apiKey: string
  authDomain: string
  projectId: string
  storageBucket: string
  messagingSenderId: string
  appId: string
}

export class FirebaseHelper {
  private static _instance: FirebaseHelper

  public static get Instance(): FirebaseHelper {
    // Do you need arguments? Make it a regular static method instead.
    return this._instance || (this._instance = new this())
  }

  private _auth: firebase.auth.Auth

  public get auth(): firebase.auth.Auth {
    return this._auth
  }

  private _firebase: firebase.app.App

  public get firebase(): firebase.app.App {
    return this._firebase
  }

  public constructor() {
    const firebaseConfig: FirebaseConfigs = {
      apiKey: 'AIzaSyAnvpT7KX_RhsBESftwPDy_YP0lvbcari0',
      authDomain: 'forex-trading-application.firebaseapp.com',
      projectId: 'forex-trading-application',
      storageBucket: 'forex-trading-application.appspot.com',
      messagingSenderId: '159696631695',
      appId: '1:159696631695:web:889f0910f290954b9e7dfa',
    }
    if (firebase.apps.length === 0) {
      // Initialize Firebase
      this._firebase = firebase.initializeApp(firebaseConfig)
      // firebase.analytics()
      this._auth = firebase.auth()
    } else {
      this._firebase = firebase.app()
      this._auth = firebase.auth()
    }
  }
}
