import { Socket, connect } from 'socket.io-client'
import { WS_PORT, WS_URL } from '../config/config'
export class SocketHelper {
  private static _instance: SocketHelper
  public socket: typeof Socket

  public static get Instance(): SocketHelper {
    // Do you need arguments? Make it a regular static method instead.
    return this._instance || (this._instance = new this())
  }

  public constructor() {
    this.socket = connect(`${WS_URL}:${WS_PORT}`, {
      transports: ['websocket'],
    })
  }
}
