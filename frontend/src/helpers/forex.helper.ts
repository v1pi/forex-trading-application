import { Forex } from '../models/forex.entity'

export interface Series {
  x: Date
  y: number[]
}
export class ForexHelper {
  private static _instance: ForexHelper

  public static get Instance(): ForexHelper {
    // Do you need arguments? Make it a regular static method instead.
    return this._instance || (this._instance = new this())
  }

  getMinLimitY(listForex: Forex[]): number {
    if (listForex.length === 0) {
      return 0
    }
    const min = listForex.reduce((previous, current) =>
      previous.low > current.low ? current : previous,
    )
    return min.low - 0.00001
  }

  getMaxLimitY(listForex: Forex[]): number {
    if (listForex.length === 0) {
      return 1
    }
    const max = listForex.reduce((previous, current) =>
      previous.high < current.high ? current : previous,
    )
    return max.high + 0.00001
  }

  convertToSeries(listForex: Forex[]): Series[] {
    return listForex.map<Series>((el) => ({
      x: el.dateTime.toDate(),
      y: [el.open, el.high, el.low, el.close],
    }))
  }
}
