import { FirebaseHelper } from '../helpers/firebase.helper'
import { Wallet } from '../models/wallet.entity'
import { WalletRepository } from '../repositories/wallet.repository'

export class WalletController {
  private walletRepository: WalletRepository
  constructor() {
    this.walletRepository = new WalletRepository()
  }

  public async getWallet(): Promise<Wallet> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.walletRepository.getWallet(token, currentUser.uid)
  }

  public async deposit(amount: number): Promise<Wallet> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.walletRepository.deposit(token, currentUser.uid, amount)
  }

  public async withdraw(amount: number): Promise<Wallet> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.walletRepository.withdraw(token, currentUser.uid, amount)
  }
}
