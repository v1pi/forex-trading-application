import { ApiHelper } from '../helpers/api.helper'
import { SignUpInterface } from '../models/signup.interface'
import { Wallet } from '../models/wallet.entity'
import { SignUpRepository } from '../repositories/signup.repository'

export class SignUpController {
  private signUpRepository: SignUpRepository
  constructor() {
    this.signUpRepository = new SignUpRepository()
  }

  public async signUp(data: SignUpInterface): Promise<Wallet | undefined> {
    return this.signUpRepository.signUp(data)
  }
}
