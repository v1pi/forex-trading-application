import { FirebaseHelper } from '../helpers/firebase.helper'
import { Trade } from '../models/trade.entity'
import { TradeRepository } from '../repositories/trade.repository'

export class TradeController {
  private tradeRepository: TradeRepository
  constructor() {
    this.tradeRepository = new TradeRepository()
  }

  public async getAllTrades(): Promise<Trade[]> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.tradeRepository.getAllTrades(token, currentUser.uid)
  }

  public async getTrade(id: number): Promise<Trade> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.tradeRepository.getTrade(token, id)
  }

  public async close(id: number): Promise<string> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.tradeRepository.close(token, id)
  }

  public async open(trade: Trade): Promise<string> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.tradeRepository.open(token, trade)
  }

  public async getTradeByJobId(id: string): Promise<Trade> {
    const currentUser = FirebaseHelper.Instance.auth.currentUser
    if (!currentUser) {
      throw new Error('You are not logged.')
    }
    const token = await currentUser.getIdToken()

    return this.tradeRepository.getTradeByJobId(token, id)
  }
}
