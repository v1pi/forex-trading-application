export abstract class BaseEntity<T> {
  id!: number

  abstract fillFromJson(json: any): T
}
