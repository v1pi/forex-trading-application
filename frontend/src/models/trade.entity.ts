import { BaseEntity } from './base.entity'
import moment from 'moment-timezone'

export enum TradeStatus {
  OPEN = 0,
  CLOSE = 1,
}

export enum TradeType {
  SELL = 0,
  BUY = 1,
}

export class Trade extends BaseEntity<Trade> {
  amount!: number
  profit!: number
  uid!: string
  jobId!: string
  multiplier!: number
  type!: TradeType
  status!: TradeStatus
  open!: number
  closed!: number
  dateOpen!: moment.Moment
  dateClosed!: moment.Moment

  fillFromJson(json: any): Trade {
    this.amount = json.amount
    this.profit = json.profit
    this.uid = json.uid
    this.multiplier = json.multiplier
    this.type = json.type
    this.status = json.status
    this.open = json.open
    this.closed = json.closed
    this.dateOpen = moment(json.dateOpen)
    this.dateClosed = moment(json.dateClosed)
    this.id = json.id

    return this
  }

  public static fromJson(json: any): Trade {
    return new Trade().fillFromJson(json)
  }
}
