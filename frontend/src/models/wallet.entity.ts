import { BaseEntity } from './base.entity'

export class Wallet extends BaseEntity<Wallet> {
  uid!: string

  email!: string

  name!: string

  amount!: number

  fillFromJson(json: any): Wallet {
    this.uid = json.uid
    this.email = json.email
    this.name = json.name
    this.amount = json.amount
    this.id = json.id
    return this
  }

  public static fromJson(json: any): Wallet {
    return new Wallet().fillFromJson(json)
  }
}
