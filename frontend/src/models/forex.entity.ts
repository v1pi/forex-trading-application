import { BaseEntity } from './base.entity'
import moment from 'moment-timezone'

export class Forex extends BaseEntity<Forex> {
  open!: number
  close!: number
  high!: number
  low!: number
  dateTime!: moment.Moment

  fillFromJson(json: any): Forex {
    this.open = json.open
    this.close = json.close
    this.high = json.high
    this.low = json.low
    this.dateTime = moment(json.dateTime)
    this.id = json.id
    return this
  }

  public static fromJson(json: any): Forex {
    return new Forex().fillFromJson(json)
  }
}
