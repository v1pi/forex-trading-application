import { ApiHelper } from '../helpers/api.helper'
import { Wallet } from '../models/wallet.entity'

export class WalletRepository {
  public async getWallet(token: string, uid: string): Promise<Wallet> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${uid}/info`,
      'GET',
    )
    const newWallet = Wallet.fromJson(response.data.wallet)
    return newWallet
  }

  public async deposit(
    token: string,
    uid: string,
    amount: number,
  ): Promise<Wallet> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${uid}/${amount}/deposit`,
      'PATCH',
    )
    const newWallet = Wallet.fromJson(response.data.wallet)
    return newWallet
  }

  public async withdraw(
    token: string,
    uid: string,
    amount: number,
  ): Promise<Wallet> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${uid}/${amount}/withdraw`,
      'PATCH',
    )
    const newWallet = Wallet.fromJson(response.data.wallet)
    return newWallet
  }

  get routeName(): string {
    return 'wallets'
  }
}
