import { ApiHelper } from '../helpers/api.helper'
import { Trade } from '../models/trade.entity'

export class TradeRepository {
  public async open(token: string, trade: Trade): Promise<string> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/open`,
      'POST',
      trade,
    )
    const jobId = response.data.job
    return jobId
  }

  public async close(token: string, id: number): Promise<string> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${id}/close`,
      'PUT',
    )
    return response.data.job
  }

  public async getTrade(token: string, id: number): Promise<Trade> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${id}/info`,
      'GET',
    )
    const newTrade = Trade.fromJson(response.data.trade)
    return newTrade
  }

  public async getTradeByJobId(token: string, jobId: string): Promise<Trade> {
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${jobId}/job-info`,
      'GET',
    )
    if (response.data.job) {
      throw new Error()
    }
    const newTrade = Trade.fromJson(response.data.trade)
    return newTrade
  }

  public async getAllTrades(token: string, uid: string): Promise<Trade[]> {
    const trades: Trade[] = []
    const response = await ApiHelper.Instance.Request(
      token,
      `${this.routeName}/${uid}/all`,
      'GET',
    )
    for (const trade of response.data.trades) {
      const newTrade = Trade.fromJson(trade)
      trades.push(newTrade)
    }
    return trades
  }

  get routeName(): string {
    return 'trades'
  }
}
