import { ApiHelper } from '../helpers/api.helper'
import { SignUpInterface } from '../models/signup.interface'
import { Wallet } from '../models/wallet.entity'

export class SignUpRepository {
  public async signUp(data: SignUpInterface): Promise<Wallet | undefined> {
    const response = await ApiHelper.Instance.RequestWithoutAuth(
      `${this.routeName}/admin`,
      'POST',
      data,
    )
    if (response.data.wallet) {
      const newWallet = Wallet.fromJson(response.data.wallet)
      return newWallet
    }
  }

  get routeName(): string {
    return 'signup'
  }
}
