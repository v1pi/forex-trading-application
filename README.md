# Dafiti Forex Application

_Your solution for do trades_<br><br>
Content
=================
<!--ts-->
* [Concept](#concept)
* [Database Model](#database-model)
* [Configuration](#configuration)
    * [Backend configuration](#backend-configuration)
    * [Frontend configuration](#frontend-configuration)
* [Running](#running)
    * [Backend running](#backend-running)
    * [Frontend running](#frontend-running)
* [Build](#build)
    * [Backend build](#backend-build)
    * [Frontend build](#frontend-build)
* [Tests](#tests)
* [Using Docker](#using-docker)
    * [Running with Docker](#running-with-docker)
    * [Tests with Docer](#tests-with-docker)
* [Routes](#routes)
    * [Trades](#trades)
        * [[POST] /trades/open](#post-tradesopen)
        * [[PUT] /trades/:id/close](#put-tradesidclose)
        * [[GET] /trades/:uid/all](#get-tradesuidall)
        * [[GET] /trades/:id/info](#get-tradesidinfo)
        * [[GET] /trades/:job-id/job-info](#get-tradesjob-idjob-info)
    * [Wallets](#wallets)
        * [[GET] /wallets/:uid/info](#get-walletsuidinfo)
        * [[PATCH] /wallets/:uid/:amount/withdraw](#patch-walletsuidamountwithdraw)
        * [[PATCH] /wallets/:uid/:amount/deposit](#patch-walletsuidamountdeposit)
    * [Sign Up](#sign-up)
        * [[POST] /signup/admin](#post-signupadmin)
* [Insomnia](#insomnia)
<!--te-->
# Concept
Daifiti is a platform for doing forex trades. You can buy and sell dollars based on GBPUSD pairs, our focus is the simple UI and strong security. Check the basic app fluxogram.
![App fluxogram](./images/fluxogram.png)
<br>
_Figure 1. App fluxogram._
<br><br>
How can be seen for all operations is needed to be logged, if don’t have an account, you can do one for free. Once logged in, you are able to enjoy all app features: see forex values of GBP/USD pair from two hours ago, buy or sell that pair, deposit and withdraw money. 
The app has a wallet, for all operations (buy or sell) is necessary to have enough money to be done. In the section Wallet has an option to deposit and withdraw USD, no other currency work on.<br><br>
For doing trades the app has a “Trades” section, where can be opened new trades or closed the already opened trades. For open a new trade, should be chosen the type, the multiplier and the amount. In type option is chosen between buy or sell pound sterling, if buying is selected, you must define the multiplier and dollar amount you want to buy in pounds. Else, if selling was selected, you must define the multiplier and how many dollars you want sell. After open a trade, your dollar amount will be discounted from your wallet, you get the value back more your profit (can be negative) after closing an opened trade.<br><br>
The profit calculation is simple, without discounts or variances and follows the following formula:<br><br>
`profit = (trade_fopen - trade_fclose) * trade_amount * trade_multiplier * α`<br><br>
Where α is 1 if trade is to buy pounds and -1 if trade is for sell dollars, trade_fopen is the forex value when the trade was opened,  trade_fclose is the forex value when the trade was closed,  trade_amount is the trade dollars amount and trade_multiplier is the trade multiplier.<br><br>
The forex values are displayed only in a candlestick chart, the chart is generated with forex values from real world data and two hours ago with a 15 minutes interval. And every 15 minutes is made a new request for new data. Candlestick chart was chosen because it has a simple way to showing important values such as open, close, high and low, in addition to having a color representation of the trend (green if close value is big than open value, or red otherwise).

# Database Model
We have the following tables in database:<br>
![Tables](./images/tables.png)
<br>
_Figure 2. Tables._<br>

This tables are unrelated, because they were migrated from mongodb. Therefore, we have redundancy information to establish a relationship. For example, the uid field in the “wallets” and “trades” tables.

# Configuration
**Important:** this project has a docker installation guide for easy deploy, if you want, see the section [Using Docker](#using-docker). If you want to do a manually configuration, continue.

The application consists of two projects, one for backend and other for frontend. They both are made using Typescript and NodeJS, but backend project use NestJS, PostgresSQL and Redis while frontend project use React. The following steps is common for both projects:<br>
Note: We assume that you are using a Linux distro.<br>
Note 2: Make sure that the following ports are free: 3000, 3001, 27017 and 5000.<br><br>
1)	Clone the project repository, to access [click here](https://gitlab.com/v1pi/forex-trading-application).
2)	Now, we need to install NodeJS. For this we will install the nvm for control the node version, so open a terminal and execute:

    `$ wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash`

3)	Close and open a new terminal and install node with:

    `$ nvm install v10.16.3`

4)	Open a terminal inside backend folder and run:

    `$ npm install`

5)	Open a terminal inside frontend folder and run:

    `$ npm install`

6)	Finished common configuration

# Backend configuration
For the backend we need to install the postres and redis, it has different ways to install and depends on your operations system, in addiction there can be many problems during its installation and configuration. So, in this project we chose to work with a Docker and Docker Compose to install postgres and redis. The Docker and Docker Compose have an extensive installation, but they have good documentation on their site on how to install. Therefore, to how to install Docker [click here](https://docs.docker.com/engine/install/) and Docker Compose [click here](https://docs.docker.com/compose/install/), you will need the docker already installed to install docker compose. The following steps is about how to create a postgres and redis container:<br>
Note: you will need to have already made the common configurations.
1)	Open a new terminal in backend project folder
2)	With docker and docker compose installed run:

    `$ npm run up:database`

3)	If you want to delete the mongodb and redis container, run:

    `$ npm run down:database`

You can change the postgres password, host and database name in docker-compose-database.yaml file at backend project root. Also, you can change the redis host and port.

Now is needed to create .env file, the application uses two different .env files: production and development. For security reasons we don&#39;t commit these files. To run this app, create a file named development.env in the root of backend folder with these values:

```
POSTGRES_PASSWORD="beribo"
POSTGRES_DB="delivery"
POSTGRES_HOST="localhost"
POSTGRES_PORT=5432
POSTGRES_USER="postgres"

REDIS_HOST="localhost"
REDIS_PORT=6379

HOST="0.0.0.0"
PORT=3001

API_FOREX_KEY="z3ccUb3zJIRhepW_K3mO"
API_FOREX_HOST="https://marketdata.tradermade.com/api/v1"
```

If you want to run this application in production mode, create a file named _production.env_ with the same fields.

If you change the _PORT_ or _HOST,_ make sure that updated your .env file in frontend root project. If you change the _POSTGRES\_\*_ make sure that postgres container is setting to do it. The same for _REDIS\_\*_ changes.

For the least, open a terminal in root backend folder and run:

`$ npm install -g @nestjs/cli`

Now is done.
## Frontend configuration

Nothing is needed beyond the common configuration. But if you want to change the host and port from API and WebSocket, create a file named _.env_ in root frontend folder with these values:
```
REACT_APP_API_URL="http://localhost"
REACT_APP_API_PORT="3001"
REACT_APP_WS_URL="ws://localhost"
REACT_APP_WS_PORT="3001"
```

# Running

Once the both applications are configured. You are able to run for the first time. It&#39;s important that running backend project before running frontend project.

## Backend running

For run the backend open a terminal in backend folder and run:

`$ npm run start`

If your postgres and redis container is not running, execute in backend folder before the above command the following line:

`$ npm run up:database`

To stop use CTRL+C in the terminal, and to stop execute the following line:

`$ npm run down:database`

## Frontend running

For run the frontend open a terminal in backend folder and run:

`$ npm run start`

You can access the site in your browser at the following URL:

http://localhost:3000/

To stop use CTRL+C in the terminal

Note: make sure that your backend application is running on localhost and on port 3001.

# Build

Once the both applications are configured. You are able to buid.

## Backend build

For do backend build open a terminal in root backend folder and run:

`$ npm run build`

Now, to run the build in development mode execute:

`$ cross-env NODE_ENV=development node ./dist/main.js`

To run in production mode, run:

`$ cross-env NODE_ENV=production node ./dist/main.js`

Note: make sure that you have created the _production.env_and _development.env_files and your postgres and redis container is running.

## Frontend build

For do frontend build open a terminal in root frontend folder and run:

`$ npm run build`

Now, to run the build run:

`$ npx serve -s build`

Note: make sure that your backend application is running on localhost and on port 3001.

# Tests

This application has only backend tests. For run, open the backend root folder and execute:

$ npm run test

# Using Docker

To make it simple, you can use docker to run and test the application. First of all, you need Docker and Docker Compose installed. For it, visit the website to learn how to install on your machine, to how to install Docker [click here](https://docs.docker.com/engine/install/) and Docker Compose [click here](https://docs.docker.com/compose/install/), you will need the docker already installed to install docker compose.

Once everything is installed clone the project repository, to access [click here](https://gitlab.com/v1pi/forex-trading-application).

Note: Make sure that the following ports are free: 3001, 5433, 6380 and 2503.

## Running with Docker

To run the application, open a terminal in root folder of your cloned repository and execute the following line:

`$ docker-compose -f ./docker-compose.yaml up -d --build`

Case you want remove the container, run:

`$ docker-compose -f ./docker-compose.yaml down`

Note: If you already run the application, you don&#39;t need to build the image again so you can only run:

`$ docker-compose -f ./docker-compose.yaml up -d`

Frontend application: http://localhost:2503/

Backend application: 
 - REST: http://localhost:3001/
 - WebSocket: ws://localhost:3001/

MongoDB database: http://localhost:6380/

Postgres database: http://localhost:5432/

## Tests with Docker

To test the application, open a terminal in root folder of your cloned repository and execute the following line:

`$ docker-compose -f ./docker-compose-test.yaml up –build`

Case you want remove the container, run:

`$ docker-compose -f ./docker-compose-test.yaml down`

# Routes

The application endpoints. The values in request and response body are mocked. For every request in the REST API other than the _signup_ route, it will be necessary to generate a JWT token and send it as a Bearer. This JWT token can be generated doing a request to firebase, you can read more in the section [Insomnia](#insomnia).

Notes:

<ul>
<li> uid: refers to the user id provided by firebase;
<li> id: refers to the document id of a given collection;
<li> job-id: refers to id in the queue;
<li> amount: refers to how many dollars.
</ul>

## Trades
Handle with trade data. Collection: trades

### [POST] /trades/open

Request body:

```json
{ 
"amount": 250,
	"multiplier": 20,
	"type": 1
}
```

Response body:

```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "job": "4"
  }
}
```

### [PUT] /trades/:id/close

Request body:

No body

Response body:

```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "job": "3"
  }
}
```

### [GET] /trades/:uid/all

Request body:

No body

Response body:

```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "trades": [
      {
        "_id": "606c699c372e9420f0878f08",
        "amount": 250,
        "multiplier": 20,
        "type": 1,
        "status": 1,
        "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
        "dateOpen": 1617717660155,
        "open": 1.38242,
        "__v": 0,
        "closed": 1.38242,
        "dateClosed": 1617717772144,
        "newAmount": 0
      }
    ]
  }
}
```

### [GET] /trades/:id/info
Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "trade": {
      "_id": "606c699c372e9420f0878f08",
      "amount": 250,
      "multiplier": 20,
      "type": 1,
      "status": 1,
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "dateOpen": 1617717660155,
      "open": 1.38242,
      "__v": 0,
      "closed": 1.38242,
      "dateClosed": 1617717772144,
      "newAmount": 0
    }
  }
}
```

### [GET] /trades/:job-id/job-info
Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "trade": {
      "amount": 250,
      "multiplier": 20,
      "type": 1,
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "status": 0,
      "dateOpen": "2021-04-14T13:06:16.554Z",
      "open": 1.37543,
      "profit": null,
      "id": 2
    }
  }
}
```

## Wallets

Handle with wallet data. Collection: wallets.

### [GET] /wallets/:uid/info
Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "wallet": {
      "_id": "606c4979be32f81150817c9c",
      "name": "Vinicius Picanço",
      "email": "vimivini@gmail.com",
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "amount": 4281.914111111111,
      "__v": 0
    }
  }
}
```

### [PATCH] /wallets/:uid/:amount/withdraw

Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "wallet": {
      "_id": "606c4979be32f81150817c9c",
      "name": "Vinicius Picanço",
      "email": "vimivini@gmail.com",
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "amount": 4281.914111111111,
      "__v": 0
    }
  }
}
```

### [PATCH] /wallets/:uid/:amount/deposit

Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "wallet": {
      "_id": "606c4979be32f81150817c9c",
      "name": "Vinicius Picanço",
      "email": "vimivini@gmail.com",
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "amount": 4281.914111111111,
      "__v": 0
    }
  }
}
```

### Sign Up

Handle with register.

### [POST] /signup/admin

Request body:

No body

Response body:
```json
{
  "error_id": -1,
  "message": "Success!",
  "error": false,
  "data": {
    "administrador": {
      "_id": "606c4979be32f81150817c9c",
      "name": "Vinicius Picanço",
      "email": "vimivini@gmail.com",
      "uid": "vdZ83Oifh0XsUDFjsNeYVgQ7vZA3",
      "amount": 5000,
      "__v": 0
    }
  }
}
```

# Insomnia
To take advantage of the endpoints already created in insomnia, just download the file that is at the root of this project _insomnia.json_ and import it into insomnia. For every request in the REST API other than the _signup_ route, it will be necessary to generate a JWT token and send it as a Bearer. To generate the token there is a route located at _Utils->generateAdminToken_, just execute the route with the email and password of your account already created and copy the &quot;idToken&quot; field of the response.

If you do not use insomnia and want to generate this token, just make a request in the following format:

```
curl --request POST \
  --url 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC21S5mRNneDH1X_ZVFRT6b5Hl4ztAt8Go' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data email=<your_email> \
  --data password=<your_password> \
  --data returnSecureToken=true
```

# Notes

We are using the Firebase Authentication System, so even you have deleted the Mongo DB data your user will remain registered. That way, you don't have to register a new user, only do the login and a new collection will be created to it.

For check the backend patterns folder [click here](https://medium.com/the-crowdlinker-chronicle/best-way-to-structure-your-directory-code-nestjs-a06c7a641401).
